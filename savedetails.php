<?php

/**
 * Save details API
 */

 error_reporting(E_ALL&~E_NOTICE);
 ob_start();

class SaveDetails{

  public function __construct(){
    $actionType = $_POST['actionType'];
    switch ($actionType){
      case "savedetails": self::saveDetails(); break;
      default : self::invalidAccess();
    }
  }

  protected function invalidAccess(){
      self::showError("1", "Invalid Access");
  }

  protected function showError($code, $message=''){
    $outputArray['message'] = $message;
    $outputArray['code'] = $code;
    $outputArray['data'] = '';
    echo json_encode($outputArray);
  }

  protected function showSuccess($data = ''){
    $outputArray['message'] = "success";
    $outputArray['code'] = "0";
    $outputArray['data'] = $data;
    echo json_encode($outputArray);
  }

  protected function saveDetails(){
      include 'include/connect.php';
        $user_id = $_POST['user_id'];
        $place_id = $_POST['place_id'];
        $address = $_POST['address'];
        $rating = $_POST['rating'];
        $phone_no = $_POST['phone_no'];
        $name = $_POST['name'];
        $latitude = $_POST['latitude'];
        $longitude = $_POST['longitude'];
        $grade = $_POST['grade'];
        $cuisine = $_POST['cuisine'];
        $reviews_count = $_POST['reviews_count'];

        $sql = "SELECT phone_no from newd WHERE phone_no = '$phone_no'";
        $stm = $db->query($sql);
        $stm1 = $stm->num_rows;

        if($stm1 > 0){
          $outputArray['news'] = "This Phone Number is already store in our database";
          self::showSuccess($outputArray);
          exit();
        } else {
          $sql = "INSERT INTO newd(user_id,place_id,address,rating,phone_no,name,latitude,longitude,grade,cuisine,reviews_count) VALUES('$user_id','$place_id','$address','$rating','$phone_no',
          '$name','$latitude','$longitude','$grade','$cuisine','$reviews_count')";
          $query = $db->query($sql);
          $outputArray['suc'] = "success delivery";
          self::showSuccess($outputArray);
          exit();
        }
  }
}

$work = new SaveDetails();

?>
