<?php

error_reporting(E_ALL&~E_NOTICE);
ob_start();
//$_POST = $_GET;
$actionType = $_POST['actionType'];
//if( false == isset($_POST['actionType']) ) {
//    $_POST = $_GET;
//    $actionType = $_POST['actionType'];
//}
//$actionType = $_POST["action"];

switch($actionType)
{

    case "login": login(); break; ///For User Login
    case "register": registration(); break; ///For user Registration
    case "forgot-password":forgot_password();break;
    case "save-preferences": save_preferences(); break; //Saving user profile
	  case "get-user-preference": fetch_user_preferences(); break; //Fetch user preference
    case "save-usr-name": save_usr_name(); break;
    case "upload-profile-pic": upload_profile_pic(); break;
    case "usr-profile": user_profile(); break;
    case "follow-usr": follow_user(); break;
    case "unfollow-usr": unfollow_user(); break;
    case "view-following": view_following(); break;
    case "view-followers": view_followers(); break;
    case "view_user_I_follow": view_user_I_follow(); break;
    case "view_user_following_me": view_user_following_me(); break;
    case "search": query(); break;
    case "query": query(); break;
    case "filter-res": filter_search(); break;
	  case "filter-res-dist": filter_search_distance(); break;
    case "comment-submit": comment_submit(); break;
    case "ftch-rstrnt-cmmnt": fetch_restaurant_comments(); break;
    case "dlt-cmmnt": delete_comment(); break;
    case "like-restaurant" : like_restaurant(); break;
    case "unlike-restaurant" : unlike_restaurant(); break;
    case "ftch-rstrnt-lik" : fetch_restaurant_likes(); break;
    case "ftch-rstrnt-usr-lik" : fetch_usr_rstrnt_likes(); break;
    case "cnt-rstrnt-lik" : count_restaurant_likes(); break;
    case "cnt-dish-lik" : count_dish_likes(); break;
    case "cnt-dish-dislik" : count_dish_dislikes(); break;
    case "cnt-rstrnt-comment" : count_comment_restaurant(); break;
    case "cnt-dish-comment" : count_comment_dish(); break;
    case "add-rtng" : add_rating(); break;
    case "get-avg-rtng" : get_avg_rating(); break;
    case "get-pop-rest": get_popular_restaurant(); break;
    case "upload-dish": upload_dish(); break;
    case "get-rest-list": get_rest_list(); break;
    case "get-usr-dish": get_dish_user(); break;
    case "get-rest-dish": get_dish_restaurant(); break;
    case "get-rest-usr-dish": get_dish_usr_restaurant(); break;
    case "get-all-dish": get_dish_all(); break;
    case "get-popular-dish": get_popular_dish(); break;
    case "get-follow-dish": get_follow_dish(); break;
    case "like-dish": like_dish(); break;
    case "unlike-dish": unlike_dish(); break;
    case "del-like-dish": del_like_dish(); break;
    case "ftch-usr-dish-lik" : fetch_usr_dish_likes(); break;
    case "dish_comment_submit": dish_comment_submit(); break;
    case "fetch_dish_comments": fetch_dish_comments(); break;
    case "fetch_user_dish_comments": fetch_user_dish_comments(); break;
    case "delete_dish_comment": delete_dish_comment(); break;
    case "get_dish_comment_usr_list" : get_dish_comment_usr_list(); break;
    case "get_dish_like_usr_list" : get_dish_like_usr_list(); break;
    case "get_dish_dislike_usr_list" : get_dish_dislike_usr_list(); break;
    case "add_restaurant" : add_restaurant(); break;
    case "upload_restaurant_pic" : upload_restaurant_pic(); break;
    case "dish_add_to_list" : dish_add_to_list(); break;
    case "dish_delete_from_list" : dish_delete_from_list(); break;
    case "fetch_user_dish_list" : fetch_user_dish_list(); break;
    case "cuisine_list" : cuisineList(); break;
    case "grade_list" : gradeList(); break;
	  case "old_query_search" : old_query(); break;
    case "post_charge": post_charge(); break;
    case "moodslist": moodslist(); break;
    case "savedetails" : saveDetails(); break;  // savedetails function new added
    default : invalidAccess();

}

/**
*save details testing api function
**/

function saveDetails(){
  include_once 'include/db.php'; // Include the database connection here

  /**
  * I used to add 3 column(user_id,place_id,address) on resturant table and use
  * count column for total_reviews count
  **/

  $user_id = $_POST['user_id'];
  $place_id = $_POST['place_id'];
  $address = $_POST['address'];
  $rating = $_POST['rating'];
  $phone = $_POST['phone'];
  $name = $_POST['User_name'];
  $latitude = $_POST['latitude'];
  $longitude = $_POST['longitude'];
  $grade = $_POST['grade'];
  $cuisine = $_POST['cuisine'];
  $reviews_count = $_POST['reviews_count'];
  $data = array();
  $table = array();
  $newTableID = array();
  $newData = array();
  $newColumn = array();
  $newValues = array();
  $table = ["name","grade","phone","cuisine","latitude","longitude","total_rating","count","place_id","user_id","address"];
  $data = [$name,$grade,$phone,$cuisine,$latitude,$longitude,$rating,$reviews_count,$place_id,$user_id,$address];

  for($i = 0 ; $i < count($data) ; $i++){
    if(!empty($data[$i])){
      array_push($newTableID,$i);
    }
  }

  for ($j = 0 ; $j < count($newTableID) ; $j++){
    $getData =  $newTableID[$j];
    $newDataFromGetData = $data[$getData];
    $newColumnFromGetCol = $table[$getData];
    $newValuesFromGetCol = ":".$table[$getData];
    array_push($newData,$newDataFromGetData);
    array_push($newColumn,$newColumnFromGetCol);
    array_push($newValues,$newValuesFromGetCol);
  }

  $newDataAfterInput = implode(",",$newData);
  $newColumnAfterInput = implode(",",$newColumn);
  $newValuesAfterInput = implode(",",$newValues);

  try {
    $stmt2 = $db->prepare("INSERT INTO restaurants ($newColumnAfterInput) VALUES ($newValuesAfterInput)");
    for($k=0 ; $k < count($newData) ; $k++){
      if($newColumn[$k] == $table[4] || $newColumn[$k] == $table[5] || $newColumn[$k] == $table[6] || $newColumn[$k] == $table[7]){
        $stmt2->bindParam($newValues[$k],$newData[$k],PDO::PARAM_INT);
      } else{
        $stmt2->bindParam($newValues[$k],$newData[$k],PDO::PARAM_STR);
      }
    }
    if($stmt2->execute()){
      $outputArray = array();
      $outputArray['success'] = "successfully done the entry";
      showSuccess($outputArray);
    } else {
      $outputArray = array();
      $code = "100";
      $outputArray['success'] = "something problem please try again!!";
      showError($code,$outputArray);
    }
  } catch(Exception $e){
    showError("100", $e->getMessage());
    $db = null;
    exit();
  }
}

function invalidAccess()
{
    showError("1", "Invalid Access");
}

function showError($code, $message='')
{
    $outputArray['message'] = $message;
    $outputArray['code'] = $code;
    $outputArray['data'] = '';
    echo json_encode($outputArray);
}

function showSuccess($data='')
{
    $outputArray['message'] = "success";
    $outputArray['code'] = "0";
    $outputArray['data'] = $data;
    echo json_encode($outputArray);
}

///Login - begin
function login(){
	$loginType = $_POST['loginType'];

    switch($loginType)
    {
        case "1": case 1: /* Manual Login */
	 		manual_login();
	 		break;
        case "2": case 2: /* Facebook Login */
			fb_login();
			break;
        case "3": case 3: /* Google plus Login */
			google_login();
			break;
        default : invalidAccess();
    }
}

function manual_login(){
	include_once("check_session.php");

    $email = $_POST['user_email'];
    $email = strip_tags($email);
    $password = $_POST['user_pwd'];

	$stmt1 = $db->prepare("SELECT signid, name, pswd FROM signup WHERE email=:email AND activated='1' LIMIT 1");
	$stmt1->bindValue(':email',$email,PDO::PARAM_STR);
	try{
		$stmt1->execute();
		$count = $stmt1->rowCount();
		if($count > 0){
			while($row = $stmt1->fetch(PDO::FETCH_ASSOC)){
				$uid = $row['signid'];
				$username = $row['name'];
				$hash = $row['pswd'];
			}

			$stmt2 = $db->prepare("SELECT syncid FROM synclogin WHERE signid=:signid LIMIT 1");
			$stmt2->bindValue(':signid',$uid,PDO::PARAM_STR);
			$stmt2->execute();
			$count = $stmt2->rowCount();
			if($count > 0){
				while($row = $stmt2->fetch(PDO::FETCH_ASSOC)){
					$syncid = $row['syncid'];
				}
			}

			$hash = trim($hash);

			if (password_verify($password, $hash)) {
				$db->query("UPDATE signup SET lastlog=now() WHERE signid='$uid' LIMIT 1");

                        $profilePicPath = "";
                        $fetchProfilePic = $db->prepare("SELECT s.name, s.path FROM profile s  WHERE s.syncid = :syncid");
                        $fetchProfilePic->bindParam(':syncid',$syncid,PDO::PARAM_STR);
                        $fetchProfilePic->execute();
			if($fetchProfilePic->rowCount() > 0){
				while($row = $fetchProfilePic->fetch(PDO::FETCH_ASSOC)){
					$profilePicPath = $row['path'];
				}
			}

		        $outputArray = array();
		        $outputArray['id'] = $syncid; //user id
		        $outputArray['signid'] = $uid; //sign id
		        $outputArray['name'] = $username; // full name
		        $outputArray['email'] = $email; // email
		        $outputArray['pic'] = $profilePicPath;
		        $outputArray["response"] = "Authenticated";
		        /*
		         * Provide any other details if required for future *
		         */
		        showSuccess($outputArray);
				exit();
			} else {
				showError("2", "Incorrect password");
				$db = null;
				exit();
			}
		}
		else{
			showError("2", "Account does not exist");
			$db = null;
			exit();
		}
	}
	catch(PDOException $e){
		showError("100", $e->getMessage());
		$db = null;
		exit();
	}

}

function fb_login(){

	include_once("check_session.php");

	$facebookId = $_POST['facebookId'];

	$stmt5 = $db->prepare("SELECT loginid, name, email FROM login WHERE fbid=:fbid LIMIT 1");
	$stmt5->bindValue(':fbid',$facebookId,PDO::PARAM_STR);
	try{
		$stmt5->execute();
		$count = $stmt5->rowCount();
		if($count > 0){
			while($row = $stmt5->fetch(PDO::FETCH_ASSOC)){
				$loginid = $row['loginid'];
				$name = $row['name'];
				$email = $row['email'];
			}
			$stmt2 = $db->prepare("SELECT syncid FROM synclogin WHERE loginid=:loginid LIMIT 1");
			$stmt2->bindValue(':loginid',$loginid,PDO::PARAM_STR);
			$stmt2->execute();
			$count = $stmt2->rowCount();
			if($count > 0){
				while($row = $stmt2->fetch(PDO::FETCH_ASSOC)){
					$syncid = $row['syncid'];
				}
			}

			$db->query("UPDATE login SET timestamp=now() WHERE loginid='$loginid' LIMIT 1");

	        $outputArray = array();
	        $outputArray['id'] = $syncid; //user id
	        $outputArray['loginid'] = $loginid; //login id
	        $outputArray['name'] = $name; // full name
	        $outputArray['email'] = $email; // email
	        $outputArray["response"] = "Authenticated";
	        showSuccess($outputArray);
			exit();

		}
		else showError("6", "This social account not associated with any account.");
	}
	catch(PDOException $e){
		showError("6", "This social account not associated with any account.");
		$db = null;
		exit();
	}


}

function google_login(){

	include_once("check_session.php");

	$googleId = $_POST['googleId'];

	$stmt5 = $db->prepare("SELECT loginid, name, email FROM login WHERE googleid=:googleid LIMIT 1");
	$stmt5->bindValue(':googleid',$googleId,PDO::PARAM_STR);
	try{
		$stmt5->execute();
		$count = $stmt5->rowCount();
		if($count > 0){
			while($row = $stmt5->fetch(PDO::FETCH_ASSOC)){
				$loginid = $row['loginid'];
				$name = $row['name'];
				$email = $row['email'];
			}
			$stmt2 = $db->prepare("SELECT syncid FROM synclogin WHERE loginid=:loginid LIMIT 1");
			$stmt2->bindValue(':loginid',$loginid,PDO::PARAM_STR);
			$stmt2->execute();
			$count = $stmt2->rowCount();
			if($count > 0){
				while($row = $stmt2->fetch(PDO::FETCH_ASSOC)){
					$syncid = $row['syncid'];
				}
			}

			$db->query("UPDATE login SET timestamp=now() WHERE loginid='$loginid' LIMIT 1");

	        $outputArray = array();
	        $outputArray['id'] = $syncid; //user id
	        $outputArray['loginid'] = $loginid; //login id
	        $outputArray['name'] = $name; // full name
	        $outputArray['email'] = $email; // email
	        $outputArray["response"] = "Authenticated";
	        showSuccess($outputArray);
			exit();

		}
		else showError("6", "This social account not associated with any account.");
	}
	catch(PDOException $e){
		showError("6", "This social account not associated with any account.");
		$db = null;
		exit();
	}
}

///Login - End


///Registration - Begin
 function registration(){
    $regType = $_POST['regType']; //1.Manual 2.fb 3.gmail
    switch($regType)
    {
        case "1": case 1: manual_registration(); break;

        case "2": case 2: fb_registration(); break;

        case "3": case 3: google_registration(); break;

        default: invalidAccess();
    }
 }

function manual_registration(){

	include_once("check_session.php");

	$full_name = strip_tags($_POST['user_name']);
	$email = strip_tags($_POST['user_email']);
	$pass = $_POST['user_pwd'];

	//// query to check if email is in the db already ////
	try{
		$stmt = $db->prepare("SELECT email FROM signup WHERE email=:email LIMIT 1");
		$stmt->bindValue(':email',$email,PDO::PARAM_STR);
		$stmt->execute();
		$count = $stmt->rowCount();
	}
	catch(PDOException $e){
		showError("100", $e->getMessage());
		$db = null;
		exit();
	}
	///Check if email is in the db already ////
	if($count > 0){
		showError("3", "Email already exist");
		$db = null;
		exit();
	}

	//// Encrypt password ////
	$user_pwd = password_hash($pass, PASSWORD_DEFAULT)."\n";

	try{
		$db->beginTransaction();
		$ipaddress = getenv('REMOTE_ADDR');
		$stmt2 = $db->prepare("INSERT INTO signup (name, email, pswd, activated, lastlog, ipaddress)
		VALUES (:full_name, :email, :pswd, '1', now(), :ipaddress)");
		$stmt2->bindParam(':full_name', $full_name, PDO::PARAM_STR);
		$stmt2->bindParam(':email',$email,PDO::PARAM_STR);
		$stmt2->bindParam(':pswd',$user_pwd,PDO::PARAM_STR);
		$stmt2->bindParam(':ipaddress',$ipaddress,PDO::PARAM_INT);
		$stmt2->execute();
		/// Get the last id inserted to the db which is now this users id for activation ////
		$lastId = $db->lastInsertId();
		$token = rand(999999999,9999999999999999);
		$stmt3 = $db->prepare("INSERT INTO activate (user, token) VALUES ('$lastId', :token)");
		$stmt3->bindValue(':token',$token,PDO::PARAM_STR);
		$stmt3->execute();

		$stmt5 = $db->prepare("SELECT syncid FROM synclogin WHERE email=:email LIMIT 1");
		$stmt5->bindValue(':email',$email,PDO::PARAM_STR);
		try{
			$stmt5->execute();
			$count = $stmt5->rowCount();
			if($count > 0){
				while($row = $stmt5->fetch(PDO::FETCH_ASSOC)){
					$syncid = $row['syncid'];
				}
				$updateSQL = $db->prepare("UPDATE synclogin SET signid=:signid WHERE syncid=:syncid LIMIT 1");
				$updateSQL->bindParam(':syncid',$syncid,PDO::PARAM_STR);
				$updateSQL->bindParam(':signid',$lastId,PDO::PARAM_STR);
				$updateSQL->execute();
			}else{
				//// insert into synclogin
				$loginid = 1;
				$stmt4 = $db->prepare("INSERT INTO synclogin (signid,loginid, name, email)
				VALUES (:signid, :loginid, :full_name, :email)");
				$stmt4->bindParam(':signid',$lastId,PDO::PARAM_STR);
				$stmt4->bindParam(':loginid',$loginid,PDO::PARAM_STR);
				$stmt4->bindParam(':full_name', $full_name, PDO::PARAM_STR);
				$stmt4->bindParam(':email',$email,PDO::PARAM_STR);
				$stmt4->execute();
				$syncid = $db->lastInsertId();

				//create user directory
				try{
					if(!is_dir("./userdirectory/$syncid")){
						if(!mkdir("./userdirectory/$syncid")){
							showError("7", "Sorry, user account could not be created. Contact system admin.");
							$db = null;
							exit();
						}else{
							if(!is_dir("./userdirectory/$syncid/dishes")){
								if(!mkdir("./userdirectory/$syncid/dishes")){
									showError("7", "Sorry, user account could not be created. Contact system admin.");
									$db = null;
									exit();
								}
							}
							if(!is_dir("./userdirectory/$syncid/profile")){
								if(!mkdir("./userdirectory/$syncid/profile")){
									showError("7", "Sorry, user account could not be created. Contact system admin.");
									$db = null;
									exit();
								}
							}
							if(!is_dir("./userdirectory/$syncid/restaurants")){
								if(!mkdir("./userdirectory/$syncid/restaurants")){
									showError("7", "Sorry, user account could not be created. Contact system admin.");
									$db = null;
									exit();
								}
							}
						}
					}
				}catch(Exception $e){
					showError("100", $e->getMessage());
					$db = null;
					exit();
				}
			}
		}catch(Exception $e){
			showError("100", $e->getMessage());
			$db = null;
			exit();
		}

		//// Send email activation to the new user ////
		$from = "From: Auto Resposder @ Fusche <anoop@yahoo.com>";
		$subject = "IMPORTANT: Activate your Fusche account";
		$link = 'http://www.fusche.com/activate.php?user='.$lastId.'&token='.$token.'';
		//// Start Email Body ////
		$message = "
		Thanks for registering an account at fusche!
		Please click the link below to confirm your identity and get started.

		$link
		";
		//// Set headers ////
		$headers = 'MIME-Version: 1.0' . "rn";
		$headers .= "Content-type: textrn";
		$headers .= "From: $fromrn";
		/// Send the email now ////
		mail($email, $subject, $message, $headers, '-f noreply@fusche.com');
		//mail($email1, $subject, $message, $headers, '-f noreply@your-email.com');
		$db->commit();

	    $outputArray = array();
	    $outputArray['id'] = $syncid; //user id
	    $outputArray['signid'] = $uid; //sign id
	    $outputArray['name'] = $username; // full name
	    $outputArray['email'] = $email; // email
	    $outputArray["response"] = "Thanks for joining! Check your email in a few moments to activate your account so that you may log in.!";
	    /*
	     * Provide any other details if required for future *
	     */
	    showSuccess($outputArray);
		$db = null;
		exit();
	}
	catch(PDOException $e){
		$db->rollBack();
		showError("100", $e->getMessage());
		$db = null;
		exit();
	}

}

function moodslist(){
    include_once("check_session.php");
    $stmt = $db->prepare("SELECT DISTINCT mood FROM moods_cuisine");
	$stmt->execute();
    $count = $stmt->rowCount();
    $arrMoods = array();
	if($count > 0){
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			$arrMoods[] = $row['mood'];
		}
    }

    showSuccess($arrMoods);
	$db = null;
	exit();
}


function fb_registration(){

 	include_once("check_session.php");

 	$facebookId = $_POST['facebookId'];

 	$name = strip_tags($_POST['user_name']);
	$email = strip_tags($_POST['user_email']);

	$stmt1 = $db->prepare("SELECT loginid, name, email FROM login WHERE email=:email LIMIT 1");
	$stmt1->bindValue(':email',$email,PDO::PARAM_STR);
	try{
		$stmt1->execute();
		$count = $stmt1->rowCount();
		if($count > 0){
			while($row = $stmt1->fetch(PDO::FETCH_ASSOC)){
				$loginid = $row['loginid'];
			}
			$stmt5 = $db->prepare("SELECT loginid, name, email FROM login WHERE email=:email AND fbid=:fbid LIMIT 1");
			$stmt5->bindValue(':email',$email,PDO::PARAM_STR);
			$stmt5->bindValue(':fbid',$facebookId,PDO::PARAM_STR);
			try{
				$stmt5->execute();
				$count = $stmt5->rowCount();
				if($count > 0){
					$stmt2 = $db->prepare("SELECT syncid FROM synclogin WHERE loginid=:loginid LIMIT 1");
					$stmt2->bindValue(':loginid',$loginid,PDO::PARAM_STR);
					$stmt2->execute();
					$count = $stmt2->rowCount();
					if($count > 0){
						while($row = $stmt2->fetch(PDO::FETCH_ASSOC)){
							$syncid = $row['syncid'];
						}
					}

					$db->query("UPDATE login SET timestamp=now() WHERE loginid='$loginid' LIMIT 1");

					showError("3", "Already registered with Facebook id");
					$db = null;
					exit();

				}else{

					$db->beginTransaction();
					$updateSQL = $db->prepare("UPDATE login SET fbid=:fbid, timestamp=now() WHERE loginid=:loginid LIMIT 1");
					$updateSQL->bindParam(':fbid',$facebookId,PDO::PARAM_STR);
					$updateSQL->bindParam(':loginid',$loginid,PDO::PARAM_STR);
					$updateSQL->execute();

					$stmt3 = $db->prepare("SELECT syncid FROM synclogin WHERE loginid=:loginid LIMIT 1");
					$stmt3->bindValue(':loginid',$loginid,PDO::PARAM_STR);
					$stmt3->execute();
					$count = $stmt3->rowCount();
					if($count > 0){
						while($row = $stmt3->fetch(PDO::FETCH_ASSOC)){
							$syncid = $row['syncid'];
						}
					}

					$db->commit();
				    $outputArray = array();
				    $outputArray['id'] = $syncid; //user id
				    $outputArray['loginid'] = $loginid; //sign id
				    $outputArray['name'] = $name; // full name
				    $outputArray['email'] = $email; // email
				    $outputArray['facebookId'] = $facebookId; // email
				    $outputArray["response"] = "Registration with Facebook successful!";
				    showSuccess($outputArray);
					$db = null;
					exit();

				}
			}
			catch(PDOException $e){
				showError("100", $e->getMessage());
				$db = null;
				exit();
			}
		}else{

			//FB reg begin

			try{
				$db->beginTransaction();
				$stmt2 = $db->prepare("INSERT INTO login (name, email, timestamp, fbid)
				VALUES (:name, :email, now(), :fbid)");
				$stmt2->bindParam(':name', $name, PDO::PARAM_STR);
				$stmt2->bindParam(':email',$email,PDO::PARAM_STR);
				$stmt2->bindParam(':fbid',$facebookId,PDO::PARAM_INT);
				$stmt2->execute();
				/// Get the last id inserted to the db which is now this users id for activation ////
				$loginid = $db->lastInsertId();
				//// insert into synclogin
				$stmt5 = $db->prepare("SELECT syncid FROM synclogin WHERE email=:email LIMIT 1");
				$stmt5->bindValue(':email',$email,PDO::PARAM_STR);
				try{
					$stmt5->execute();
					$count = $stmt5->rowCount();
					if($count > 0){
						while($row = $stmt5->fetch(PDO::FETCH_ASSOC)){
							$syncid = $row['syncid'];
						}
						$updateSQL = $db->prepare("UPDATE synclogin SET loginid=:loginid WHERE syncid=:syncid LIMIT 1");
						$updateSQL->bindParam(':syncid',$syncid,PDO::PARAM_STR);
						$updateSQL->bindParam(':loginid',$loginid,PDO::PARAM_STR);
						$updateSQL->execute();

						$db->commit();
					    $outputArray = array();
					    $outputArray['id'] = $syncid; //user id
					    $outputArray['loginid'] = $loginid; //sign id
					    $outputArray['name'] = $name; // full name
					    $outputArray['email'] = $email; // email
					    $outputArray['facebookId'] = $facebookId; // email
					    $outputArray["response"] = "Registration with Facebook successful!";
					    showSuccess($outputArray);
						$db = null;
						exit();

					}else{
						$signid = 1;
						$stmt4 = $db->prepare("INSERT INTO synclogin (loginid,signid, name, email)
						VALUES ('$loginid', :signid, :full_name, :email)");
						$stmt4->bindParam(':signid',$signid,PDO::PARAM_STR);
						$stmt4->bindParam(':full_name', $name, PDO::PARAM_STR);
						$stmt4->bindParam(':email',$email,PDO::PARAM_STR);
						$stmt4->execute();
						$syncid = $db->lastInsertId();

						$_POST['loginid'] = $loginid;
						$_POST['name'] = $name;
						$_POST['facebookId'] = $facebookId;
						$_POST['email'] = $email;
						$_POST['syncid'] = $syncid;

						//create user directory
						try{
							if(!is_dir("./userdirectory/$syncid")){
								if(!mkdir("./userdirectory/$syncid")){
									showError("7", "Sorry, user account could not be created. Contact system admin.");
									$db = null;
									exit();
								}else{
									if(!is_dir("./userdirectory/$syncid/dishes")){
										if(!mkdir("./userdirectory/$syncid/dishes")){
											showError("7", "Sorry, user account could not be created. Contact system admin.");
											$db = null;
											exit();
										}
									}
									if(!is_dir("./userdirectory/$syncid/profile")){
										if(!mkdir("./userdirectory/$syncid/profile")){
											showError("7", "Sorry, user account could not be created. Contact system admin.");
											$db = null;
											exit();
										}
									}
									if(!is_dir("./userdirectory/$syncid/restaurants")){
										if(!mkdir("./userdirectory/$syncid/restaurants")){
											showError("7", "Sorry, user account could not be created. Contact system admin.");
											$db = null;
											exit();
										}
									}
								}
							}
						}catch(Exception $e){
							showError("100", $e->getMessage());
							$db = null;
							exit();
						}

						$db->commit();
					    $outputArray = array();
					    $outputArray['id'] = $syncid; //user id
					    $outputArray['loginid'] = $loginid; //sign id
					    $outputArray['name'] = $name; // full name
					    $outputArray['email'] = $email; // email
					    $outputArray['facebookId'] = $facebookId; // email
					    $outputArray["response"] = "Registration with Facebook successful!";
					    showSuccess($outputArray);
						$db = null;
						exit();
					}
				}catch(Exception $e){
					$db->rollBack();
					showError("100", $e->getMessage());
					$db = null;
					exit();
				}
			}
			catch(PDOException $e){
			$db->rollBack();
			showError("100", $e->getMessage());
			$db = null;
			exit();
			}

			//FB reg end
		}
	}
	catch(PDOException $e){
		showError("100", $e->getMessage());
		$db = null;
		exit();
	}

}

function google_registration(){
 	include_once("check_session.php");

 	$googleId = $_POST['googleId'];

 	$name = strip_tags($_POST['user_name']);
	$email = strip_tags($_POST['user_email']);

	$stmt1 = $db->prepare("SELECT loginid, name, email FROM login WHERE email=:email LIMIT 1");
	$stmt1->bindValue(':email',$email,PDO::PARAM_STR);
	try{
		$stmt1->execute();
		$count = $stmt1->rowCount();
		if($count > 0){
			while($row = $stmt1->fetch(PDO::FETCH_ASSOC)){
				$loginid = $row['loginid'];
			}
			$stmt5 = $db->prepare("SELECT loginid, name, email FROM login WHERE email=:email AND googleid=:googleid LIMIT 1");
			$stmt5->bindValue(':email',$email,PDO::PARAM_STR);
			$stmt5->bindValue(':googleid',$googleId,PDO::PARAM_STR);
			try{
				$stmt5->execute();
				$count = $stmt5->rowCount();
				if($count > 0){
					$stmt2 = $db->prepare("SELECT syncid FROM synclogin WHERE loginid=:loginid LIMIT 1");
					$stmt2->bindValue(':loginid',$loginid,PDO::PARAM_STR);
					$stmt2->execute();
					$count = $stmt2->rowCount();
					if($count > 0){
						while($row = $stmt2->fetch(PDO::FETCH_ASSOC)){
							$syncid = $row['syncid'];
						}
					}

					$db->query("UPDATE login SET timestamp=now() WHERE loginid='$loginid' LIMIT 1");

					showError("3", "Already registered with Google id");
					$db = null;
					exit();

				}else{

					$db->beginTransaction();
					$updateSQL = $db->prepare("UPDATE login SET googleid=:googleid, timestamp=now() WHERE loginid=:loginid LIMIT 1");
					$updateSQL->bindParam(':googleid',$googleId,PDO::PARAM_STR);
					$updateSQL->bindParam(':loginid',$loginid,PDO::PARAM_STR);
					$updateSQL->execute();

					$stmt3 = $db->prepare("SELECT syncid FROM synclogin WHERE loginid=:loginid LIMIT 1");
					$stmt3->bindValue(':loginid',$loginid,PDO::PARAM_STR);
					$stmt3->execute();
					$count = $stmt3->rowCount();
					if($count > 0){
						while($row = $stmt3->fetch(PDO::FETCH_ASSOC)){
							$syncid = $row['syncid'];
						}
					}

					$db->commit();
				    $outputArray = array();
				    $outputArray['id'] = $syncid; //user id
				    $outputArray['loginid'] = $loginid; //sign id
				    $outputArray['name'] = $name; // full name
				    $outputArray['email'] = $email; // email
				    $outputArray['googleId'] = $googleId; // email
				    $outputArray["response"] = "Registration with Google successful!";
				    showSuccess($outputArray);
					$db = null;
					exit();

				}
			}
			catch(PDOException $e){
				showError("100", $e->getMessage());
				$db = null;
				exit();
			}
		}else{

			//Google reg begin

			try{
				$db->beginTransaction();
				$stmt2 = $db->prepare("INSERT INTO login (name, email, timestamp, googleid)
				VALUES (:name, :email, now(), :googleid)");
				$stmt2->bindParam(':name', $name, PDO::PARAM_STR);
				$stmt2->bindParam(':email',$email,PDO::PARAM_STR);
				$stmt2->bindParam(':googleid',$googleId,PDO::PARAM_INT);
				$stmt2->execute();
				/// Get the last id inserted to the db which is now this users id for activation ////
				$loginid = $db->lastInsertId();
				//// insert into synclogin
				$stmt5 = $db->prepare("SELECT syncid FROM synclogin WHERE email=:email LIMIT 1");
				$stmt5->bindValue(':email',$email,PDO::PARAM_STR);
				try{
					$stmt5->execute();
					$count = $stmt5->rowCount();
					if($count > 0){
						while($row = $stmt5->fetch(PDO::FETCH_ASSOC)){
							$syncid = $row['syncid'];
						}
						$updateSQL = $db->prepare("UPDATE synclogin SET loginid=:loginid WHERE syncid=:syncid LIMIT 1");
						$updateSQL->bindParam(':syncid',$syncid,PDO::PARAM_STR);
						$updateSQL->bindParam(':loginid',$loginid,PDO::PARAM_STR);
						$updateSQL->execute();

						$db->commit();
					    $outputArray = array();
					    $outputArray['id'] = $syncid; //user id
					    $outputArray['loginid'] = $loginid; //sign id
					    $outputArray['name'] = $name; // full name
					    $outputArray['email'] = $email; // email
					    $outputArray['googleId'] = $googleId; // email
					    $outputArray["response"] = "Registration with Google successful!";
					    showSuccess($outputArray);
						$db = null;
						exit();

					}else{
						$signid = 1;
						$stmt4 = $db->prepare("INSERT INTO synclogin (loginid,signid, name, email)
						VALUES ('$loginid', :signid, :full_name, :email)");
						$stmt4->bindParam(':signid',$signid,PDO::PARAM_STR);
						$stmt4->bindParam(':full_name', $name, PDO::PARAM_STR);
						$stmt4->bindParam(':email',$email,PDO::PARAM_STR);
						$stmt4->execute();
						$syncid = $db->lastInsertId();

						//create user directory
						try{
							if(!is_dir("./userdirectory/$syncid")){
								if(!mkdir("./userdirectory/$syncid")){
									showError("7", "Sorry, user account could not be created. Contact system admin.");
									$db = null;
									exit();
								}else{
									if(!is_dir("./userdirectory/$syncid/dishes")){
										if(!mkdir("./userdirectory/$syncid/dishes")){
											showError("7", "Sorry, user account could not be created. Contact system admin.");
											$db = null;
											exit();
										}
									}
									if(!is_dir("./userdirectory/$syncid/profile")){
										if(!mkdir("./userdirectory/$syncid/profile")){
											showError("7", "Sorry, user account could not be created. Contact system admin.");
											$db = null;
											exit();
										}
									}
									if(!is_dir("./userdirectory/$syncid/restaurants")){
										if(!mkdir("./userdirectory/$syncid/restaurants")){
											showError("7", "Sorry, user account could not be created. Contact system admin.");
											$db = null;
											exit();
										}
									}
								}
							}
						}catch(Exception $e){
							showError("100", $e->getMessage());
							$db = null;
							exit();
						}

						$db->commit();
					    $outputArray = array();
					    $outputArray['id'] = $syncid; //user id
					    $outputArray['loginid'] = $loginid; //sign id
					    $outputArray['name'] = $name; // full name
					    $outputArray['email'] = $email; // email
					    $outputArray['googleId'] = $googleId; // email
					    $outputArray["response"] = "Registration with Google successful!";
					    showSuccess($outputArray);
						$db = null;
						exit();
					}
				}catch(Exception $e){
					$db->rollBack();
					showError("100", $e->getMessage());
					$db = null;
					exit();
				}
			}
			catch(PDOException $e){
			$db->rollBack();
			showError("100", $e->getMessage());
			$db = null;
			exit();
			}

			//Google reg end
		}
	}
	catch(PDOException $e){
		showError("100", $e->getMessage());
		$db = null;
		exit();
	}
}
/// Registration - End

/// Forgot Passowrd [START]
function forgot_password(){
  //include_once("connect.php");
  include_once("check_session.php");

  $strEmailAddress = isset($_POST['email'])?strip_tags(trim($_POST['email'])):'';

  if(true == empty($strEmailAddress)) {
    showError("101", "No email address found");
    $db=null;
    exit;
  }

  $strSql = "SELECT * FROM signup WHERE email LIKE '" . $strEmailAddress . "'";

  $stmt = $db->prepare($strSql);
  $stmt->execute();

  if($stmt->rowCount() != 1) {
    showError("102", "Email does not registered");
    $db=null;
    exit;
  }
    $strToken = md5("signid$" . $objRes->loginid . "#email$" . $objRes->email . "#lastlog$" . $objRes->lastlog);
    $strResetLink = "http://www.fusche.com/activate.php?token=" . $strToken;
    $objRes = $stmt->fetchObject();

    //// Send email activation to the new user ////
    $from = "From: Auto Responder @ Fusche <anoop@yahoo.com>";
    $subject = "IMPORTANT: Reset your Fusche password";
    $strUser = empty($objRes->name)?$objRes->email:$objRes->name;
    //$link = 'http://www.fusche.com/activate.php?user='.$lastId.'&token='.$token.'';
    //// Start Email Body ////
    $message = "
    Hi $strUser,

    Oops! Forgot your password? Never mind, it happens. You can retrieve your password by clicking the link below:

    $strResetLink

    If you haven't made this password reset request, ignore this mail and continue with your work. Some of your friends might have played a prank on you.";

    //// Set headers ////
    $headers = 'MIME-Version: 1.0' . "rn";
    $headers .= "Content-type: textrn";
    $headers .= "From: $from";
    /// Send the email now ////
    mail($strEmailAddress, $subject, $message, $headers, '-f noreply@fusche.com');

    $strSql = "UPDATE signup SET token = '". $strToken ."' WHERE signid = '". $objRes->signid ."'";
    $stmt = $db->prepare($strSql);
    $stmt->execute();

    $outputArray = array();
    $outputArray['email'] = $strEmailAddress; // email
    $outputArray["response"] = "Reset link sent successfully!";
    showSuccess($outputArray);
	$db = null;
	exit();

}
/// Forgot Password [END]

/// Update User Profile & Preferences - Begin

function save_preferences(){
  include_once("check_session.php");

  $bio = strip_tags($_POST['bio']);
  $health = strip_tags($_POST['health']);
  $price = strip_tags($_POST['price']);
  $distance = strip_tags($_POST['distance']);
  $syncid = strip_tags($_POST['syncid']);

  //// query to check if user entry is in the db already ////
  try{
    $stmt = $db->prepare("SELECT syncid FROM user_preference WHERE syncid=:syncid LIMIT 1");
    $stmt->bindValue(':syncid',$syncid,PDO::PARAM_STR);
    $stmt->execute();
    $count = $stmt->rowCount();
  }
  catch(PDOException $e){
	showError("100", $e->getMessage());
	$db = null;
	exit();
  }
  ///Check if email is in the db already ////
  if($count > 0){
     try{
        $stmt1 = $db->prepare("UPDATE user_preference SET health_rating=:health, price_range=:price, distance=:distance, bio=:bio  WHERE syncid=:syncid");
        $stmt1->bindParam(':health',$health);
        $stmt1->bindParam(':price',$price);
        $stmt1->bindParam(':distance',$distance);
        $stmt1->bindParam(':bio',$bio);
        $stmt1->bindParam(':syncid',$syncid);
        $stmt1->execute();

        $outputArray = array();
        $outputArray['id'] = $syncid;
        $outputArray['bio'] = $bio;
        $outputArray['health'] = $health;
        $outputArray['price'] = $price;
        $outputArray['distance'] = $distance;
        $outputArray["response"] = "Profile update successful";
        showSuccess($outputArray);
		exit();
      }
      catch(PDOException $e){
		showError("100", $e->getMessage());
		$db = null;
		exit();
      }
  }else{
     try{

        $stmt4 = $db->prepare("INSERT INTO user_preference (health_rating, price_range, distance, bio,  syncid)
        VALUES ( :health, :price, :distance, :bio, :syncid )");
        $stmt4->bindParam(':health',$health,PDO::PARAM_STR);
        $stmt4->bindParam(':price',$price,PDO::PARAM_STR);
        $stmt4->bindParam(':distance',$distance,PDO::PARAM_STR);
        $stmt4->bindParam(':bio',$bio,PDO::PARAM_STR);
        $stmt4->bindParam(':syncid',$syncid,PDO::PARAM_STR);
        $stmt4->execute();

        $outputArray = array();
        $outputArray['id'] = $syncid;
        $outputArray['bio'] = $bio;
        $outputArray['health'] = $health;
        $outputArray['price'] = $price;
        $outputArray['distance'] = $distance;
        $outputArray["response"] = "save profile successful";
        showSuccess($outputArray);
		exit();
      }
      catch(PDOException $e){
		showError("100", $e->getMessage());
		$db = null;
		exit();
      }
  }

}

/// Update User Profile & Preferences - End

/// Fetch User Profile & Preferences - Begin

function fetch_user_preferences(){
  include_once("check_session.php");

  $syncid = strip_tags($_POST['syncid']);

  //// query to check if user entry is in the db already ////
  try{
    $stmt = $db->prepare("SELECT * FROM user_preference WHERE syncid=:syncid LIMIT 1");
    $stmt->bindValue(':syncid',$syncid,PDO::PARAM_STR);
    $stmt->execute();
    $count = $stmt->rowCount();
  }
  catch(PDOException $e){
	showError("100", $e->getMessage());
	$db = null;
	exit();
  }
  ///Check if email is in the db already ////
  if($count > 0){
     try{
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray['id'] = $syncid;
        $outputArray['output'] = $result;

        showSuccess($outputArray);
		exit();
      }
      catch(PDOException $e){
		showError("100", $e->getMessage());
		$db = null;
		exit();
      }
  }

}

/// Fetch User Profile & Preferences - End
//New query begins

function query(){
     include_once("check_session.php");

    $keyword = strip_tags($_POST['searchBox']);
    $keyword = trim($keyword);
    $cuisine = strip_tags($_POST['cuisine']);
    $cuisine = strtolower(trim($cuisine));
    $syncid = $_POST['syncid'];
	$price = $_POST['price'];
	$distance = $_POST['distance'];
	$grade = $_POST['grade'];
	$latitude = $_POST['latitude'];
	$longitude = $_POST['longitude'];

    $keyword = remove_accent($keyword);


    $keyword = preg_replace('/[^a-zA-Z0-9\s]+/', '', $keyword);
    $keyword = preg_replace('/\s\s+/', ' ', $keyword);

    $keys = explode(" ", strtolower(trim($keyword)));
    //echo "keyword : ", $keyword;
    array_unshift($keys, $keyword);
   // print_r($keys);
   // $return["keys"] = $keys;
   // print_r($return);
    $val = "";

    try{


        $query = "SELECT r.restaurantid, r.name, street, building, city, phone, price, cuisine, grade, latitude, longitude, r.total_rating, r.total_rating, r.count, r.zip, ( 3959 * acos( cos( radians(:latitude) ) * cos( radians(latitude) ) * cos( radians(longitude) - radians(:longitude) ) + sin( radians(:latitude) ) * sin( radians(latitude) ) ) ) AS distance FROM restaurants r WHERE ";
        if($distance != "")
			$query .= " distance < :distance";
		if($price != "" && $distance != "")
			$query .= " and price = :price";
		else if($price != "" && $distance == "")
			$query .= " price = :price";
		if($grade != "" && ($distance != "" || $price != ""))
			$query .= " and grade = :grade";
		else if($grade != "")
			$query .= " grade = :grade";
		if($distance != "" || $price != "" || $grade != "")
			$query .= " and ";
		$query .= " (";
        $query .= " REPLACE(r.name,' ','') LIKE '";//.trim($keys[0])."')";
		foreach ($keys as $key => $value)
		{
		if($key != 0)
			$query .= "%".$keys[$key];
		}
		$query .= "%' OR r.zip = '" . $keyword . "')  ORDER BY distance";




        $query.= " LIMIT 100";


        $stmt2 = $db->prepare($query);
        //$stmt2->bindValue(':cuisine',$cuisine,PDO::PARAM_STR);
			$stmt2->bindValue(':longitude',$longitude,PDO::PARAM_STR);
			$stmt2->bindValue(':latitude',$latitude,PDO::PARAM_STR);
        if($distance != "")
			$stmt2->bindValue(':distance',$distance,PDO::PARAM_STR);
		if($price != "")
			$stmt2->bindValue(':price',$price,PDO::PARAM_STR);
		if($grade != "")
			$stmt2->bindValue(':grade',$grade,PDO::PARAM_STR);
            $stmt2->execute();
			$count = $stmt2->rowCount();
$outputArray = Array();

					if($count > 0){
					$result = $stmt2->fetchAll(PDO::FETCH_OBJ);
						//while($row = $stmt2->fetch(PDO::FETCH_ASSOC)){
						$outputArray['searchBox'] = $keyword;
                $outputArray['cuisine'] = $cuisine;
				$outputArray['price'] = $price;
            $outputArray['grade'] = $grade;
			$outputArray['distance'] = $distance;
							$outputArray["results"] = $result;//['restaurantid'];
						//}
						}
           else{

		   $outputArray['searchBox'] = $keyword;
                $outputArray['cuisine'] = $cuisine;
				$outputArray['price'] = $price;
            $outputArray['grade'] = $grade;
			$outputArray['distance'] = $distance;
							$outputArray["results"] = "No results";
		   }

			showSuccess($outputArray);
//$return["value"] =$outputArray;
       // echo json_encode($outputArray);


	        $db = null;
	        exit();
    }catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }

}




////////

function query1(){
     include_once("check_session.php");

    $keyword = strip_tags($_POST['searchBox']);
    $keyword = trim($keyword);
    $cuisine = strip_tags($_POST['cuisine']);
    $cuisine = strtolower(trim($cuisine));
    $syncid = $_POST['syncid'];

    $keyword = remove_accent($keyword);


    $keyword = preg_replace('/[^a-zA-Z0-9\s]+/', '', $keyword);
    $keyword = preg_replace('/\s\s+/', ' ', $keyword);
    //$keyword = preg_replace('\'', ' ', $keyword);

    //pagedata=re.sub(r'[^"a-z0-9"]+',' ',clean)
    //pagedata=re.sub(r'[\"$]+',' ',pagedata)   #Replace all non alpha numeric characters with 'space'

    $keys = explode(" ", strtolower(trim($keyword)));
    $return["keys"] = $keys;

    $val = "";

    try{
        $res = [];
       /* if($keyword != "" or $keyword != null){
            $myfile = fopen("name_index.txt", "r") or die("Unable to open file!");
            // Output one line until end-of-file
            while(!feof($myfile)) {
              $val = fgets($myfile);
              $pieces = explode("|", $val);

              foreach ($keys as $key => $value) {
                if($value == $pieces[0]){
                    $piece = explode(" ", trim($pieces[1]));
                    $res[$value] = $piece;
                    //array_push($cus, sizeof($piece));
                }
              }
          }
        }
        if($cuisine != "" or $cuisine != null){
            $myfile = fopen("cuisine_index.txt", "r") or die("Unable to open file!");
            // Output one line until end-of-file
            $cus = [];
            while(!feof($myfile)) {
              $val = fgets($myfile);
              $pieces = explode("|", $val);
              array_push($cus, strtolower(trim($pieces[0])));
                if($cuisine == strtolower(trim($pieces[0]))){
                    $return["cus1"] =  $cuisine;
                    $pieces = explode(" ", trim($pieces[1]));
                    if(!array_key_exists($cuisine,$res)){
                        $res[$cuisine] = $pieces;
                    }else{

                    }
                }
            }
        }*/

       // fclose($myfile);

     /*   $t =  getListOfMatches($res);
        $cnt=0;
        foreach($t as $k => $v){
            $id[$cnt] = $k;
            $occ[$cnt] = $v;
            $cnt++;
        }

        $max=[];
        $max_occ = 0;
        foreach ($occ as $key => $value) {
            if($value>=$max_occ){
                $max_occ = $value;
                $max[$key]=$id[$key];
            }
        }
        $return["max"] =  $max;

        if(sizeof($max) == 0) {
                $outputArray = array();
                $outputArray['searchBox'] = $keyword;
                $outputArray['cuisine'] = $cuisine;
                $outputArray["results"] = "No results!";
                showSuccess($outputArray);
                $db = null;
                exit();
        }*/

    //   try{
          /*  $stmt = $db->prepare("INSERT INTO search_history (search_tag, cuisine_tag,  syncid, time)
            VALUES (:keyword, :cuisine, :syncid , now())");
            $stmt->bindParam(':keyword',$keyword,PDO::PARAM_STR);
            $stmt->bindParam(':cuisine',$cuisine,PDO::PARAM_STR);
            $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
            $stmt->execute();
*/

			 $query = "SELECT r.restaurantid, r.name, street, building, city, phone, price, cuisine, grade, latitude, longitude, r.total_rating, r.total_rating, r.count FROM restaurants r WHERE";
        $query .= " (";
        $query .= " REPLACE(r.name,' ','') LIKE '";//.trim($keys[0])."')";
		foreach ($keys as $key => $value)
		{
		if($key != 0)
			$query .= "%".$keys[$key];
		}
		$query .= "%')  GROUP BY latitude, longitude";




       // $query.= " LIMIT 200";

          /*  $query = "SELECT r.restaurantid, r.name, street, building, city, phone, price, cuisine, grade, latitude, longitude, r.total_rating, r.total_rating, r.count FROM restaurants r WHERE";
*/
      //      $numItems = count($max);
       //     $i = 0;
        /*    foreach ($max as $key => $value) {
                if($key == 0){
                    if(trim($value) != ""){
                        $query .= " (";
                        $query .= " r.restaurantid='".trim($value)."'";
                    }
                }else{
                    if(trim($value) != ""){
                        $query .= " or ";
                        $query .= " r.restaurantid='".trim($value)."' ";
                    }
                }
                if(++$i === $numItems) {
                    if(trim($value) != ""){
                        $query .= " )";
                    }
                }

            }*/
            $query.= " LIMIT 100";
            $return["query"] = $query;

            $stmt2 = $db->prepare($query);
            //$stmt2->bindValue(':cuisine',$cuisine,PDO::PARAM_STR);

            // $stmt2->bindValue(':start',$start,PDO::PARAM_INT);
            // $stmt2->bindValue(':end',$end,PDO::PARAM_INT);
            try{
                $stmt2->execute();
                $result = $stmt2->fetchAll(PDO::FETCH_OBJ);
                //$result = $stmt2->fetchAll(PDO::FETCH_ASSOC);
                $outputArray = array();
                $outputArray['searchBox'] = $keyword;
                $outputArray['cuisine'] = $cuisine;
                $outputArray["results"] = $result;
                showSuccess($outputArray);
                $db = null;
                exit();
            }
            catch(PDOException $e){
                showError("100", $e->getMessage());
                $db = null;
                exit();
            }


        }
        catch(PDOException $e){
            showError("100", $e->getMessage());
            $db = null;
            exit();
        }
 try
 {
        $return["value"] =$res;
        echo json_encode($return);
        $db = null;
        exit();
    }catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }

}



//New query ends
/// Query � Search for Restaurant/Cuisine - Begin

function old_query(){
     include_once("check_session.php");

    $keyword = strip_tags($_POST['searchBox']);
    $keyword = trim($keyword);
    $cuisine = strip_tags($_POST['cuisine']);
    $cuisine = strtolower(trim($cuisine));
    $syncid = $_POST['syncid'];

    $keyword = remove_accent($keyword);


    $keyword = preg_replace('/[^a-zA-Z0-9\s]+/', '', $keyword);
    $keyword = preg_replace('/\s\s+/', ' ', $keyword);
    //$keyword = preg_replace('\'', ' ', $keyword);

    //pagedata=re.sub(r'[^"a-z0-9"]+',' ',clean)
    //pagedata=re.sub(r'[\"$]+',' ',pagedata)   #Replace all non alpha numeric characters with 'space'

    $keys = explode(" ", strtolower(trim($keyword)));
    $return["keys"] = $keys;

    $val = "";

    try{
        $res = [];
        if($keyword != "" or $keyword != null){
            $myfile = fopen("name_index.txt", "r") or die("Unable to open file!");
            // Output one line until end-of-file
            while(!feof($myfile)) {
              $val = fgets($myfile);
              $pieces = explode("|", $val);

              foreach ($keys as $key => $value) {
                if($value == $pieces[0]){
                    $piece = explode(" ", trim($pieces[1]));
                    $res[$value] = $piece;
                    //array_push($cus, sizeof($piece));
                }
              }
          }
        }
        if($cuisine != "" or $cuisine != null){
            $myfile = fopen("cuisine_index.txt", "r") or die("Unable to open file!");
            // Output one line until end-of-file
            $cus = [];
            while(!feof($myfile)) {
              $val = fgets($myfile);
              $pieces = explode("|", $val);
              array_push($cus, strtolower(trim($pieces[0])));
                if($cuisine == strtolower(trim($pieces[0]))){
                    $return["cus1"] =  $cuisine;
                    $pieces = explode(" ", trim($pieces[1]));
                    if(!array_key_exists($cuisine,$res)){
                        $res[$cuisine] = $pieces;
                    }else{

                    }
                }
            }
        }

        fclose($myfile);

        $t =  getListOfMatches($res);
        $cnt=0;
        foreach($t as $k => $v){
            $id[$cnt] = $k;
            $occ[$cnt] = $v;
            $cnt++;
        }

        $max=[];
        $max_occ = 0;
        foreach ($occ as $key => $value) {
            if($value>=$max_occ){
                $max_occ = $value;
                $max[$key]=$id[$key];
            }
        }
        $return["max"] =  $max;

        if(sizeof($max) == 0) {
                $outputArray = array();
                $outputArray['searchBox'] = $keyword;
                $outputArray['cuisine'] = $cuisine;
                $outputArray["results"] = "No results!";
                showSuccess($outputArray);
                $db = null;
                exit();
        }

       try{
            $stmt = $db->prepare("INSERT INTO search_history (search_tag, cuisine_tag,  syncid, time)
            VALUES (:keyword, :cuisine, :syncid , now())");
            $stmt->bindParam(':keyword',$keyword,PDO::PARAM_STR);
            $stmt->bindParam(':cuisine',$cuisine,PDO::PARAM_STR);
            $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
            $stmt->execute();

            $query = "SELECT r.restaurantid, r.name, street, building, city, phone, price, cuisine, grade, latitude, longitude, r.total_rating, r.total_rating, r.count FROM restaurants r WHERE";

            $numItems = count($max);
            $i = 0;
            foreach ($max as $key => $value) {
                if($key == 0){
                    if(trim($value) != ""){
                        $query .= " (";
                        $query .= " r.restaurantid='".trim($value)."'";
                    }
                }else{
                    if(trim($value) != ""){
                        $query .= " or ";
                        $query .= " r.restaurantid='".trim($value)."' ";
                    }
                }
                if(++$i === $numItems) {
                    if(trim($value) != ""){
                        $query .= " )";
                    }
                }

            }
            $query.= " LIMIT 200";
            $return["query"] = $query;

            $stmt2 = $db->prepare($query);
            $stmt2->bindValue(':cuisine',$cuisine,PDO::PARAM_STR);

            // $stmt2->bindValue(':start',$start,PDO::PARAM_INT);
            // $stmt2->bindValue(':end',$end,PDO::PARAM_INT);
            try{
                $stmt2->execute();
                $result = $stmt2->fetchAll(PDO::FETCH_OBJ);
                //$result = $stmt2->fetchAll(PDO::FETCH_ASSOC);
                $outputArray = array();
                $outputArray['searchBox'] = $keyword;
                $outputArray['cuisine'] = $cuisine;
                $outputArray["results"] = $result;
                showSuccess($outputArray);
                $db = null;
                exit();
            }
            catch(PDOException $e){
                showError("100", $e->getMessage());
                $db = null;
                exit();
            }


        }
        catch(PDOException $e){
            showError("100", $e->getMessage());
            $db = null;
            exit();
        }

        $return["value"] =$res;
        echo json_encode($return);
        $db = null;
        exit();
    }catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }

}

function getListOfMatches($res){
    $t = [];
    if(sizeof($res)>1){

        $max_val = 0;
        $min_val = 999999999999999;


      foreach ($res as $key => $value) {
        if($max_val<$value[sizeof($value)-1]){
            $max_val = $value[sizeof($value)-1];
        }

        if($min_val>$value[0]){
            $min_val = $value[0];
        }
        foreach ($value as $k => $v) {
            if(!array_key_exists($v,$t)){
                $t[trim($v)]=1;
            }else{
                ++$t[trim($v)];
            }
        }

      }
      return $t;


    }else{
      foreach ($res as $key => $value) {
        foreach ($value as $k => $v) {
            $t[trim($v)]=1;
        }
      }
      return $t;
    }
}


function remove_accent($str)
{
  $a = array('�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', '�', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', '?', '?', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', '?', '?', 'L', 'l', 'N', 'n', 'N', 'n', 'N', 'n', '?', 'O', 'o', 'O', 'o', 'O', 'o', '�', '�', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', '�', '�', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', '�', 'Z', 'z', 'Z', 'z', '�', '�', '?', '�', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', '?', '?', '?', '?', '?', '?');
  $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
  return str_replace($a, $b, $str);
}

/// Query � Search for Restaurant/Cuisine - End

/// Search �  Additional Search for Restaurant/Cuisine - Begin
function search(){

  include_once("check_session.php");

    $return = $_POST;

    $keyword = strip_tags($_POST['searchBox']);
    $keyword = trim($keyword);
    $cuisine = strip_tags($_POST['cuisine']);
    $cuisine = trim($cuisine);
    $cuisine = '%'.$cuisine.'%';
    $health = strip_tags($_POST['health']);
    $price = strip_tags($_POST['price']);
    $distance = strip_tags($_POST['distance']);
    $start = (int)strip_tags($_POST['start']);
    $end = (int)strip_tags($_POST['end']);
    $lat = $_POST['lat'];
    $lng = $_POST['lng'];
    $syncid = $_POST['syncid'];

    $keys = explode(" ", trim($keyword));
    $return["keys"] = $keys;

    try{

        $stmt = $db->prepare("INSERT INTO search_history (health_rating, price_range, distance, search_tag, cuisine_tag,  syncid, time)
        VALUES ( :health, :price, :distance, :keyword, :cuisine, :syncid , now())");
        $stmt->bindParam(':health',$health,PDO::PARAM_STR);
        $stmt->bindParam(':price',$price,PDO::PARAM_STR);
        $stmt->bindParam(':distance',$distance,PDO::PARAM_STR);
        $stmt->bindParam(':keyword',$keyword,PDO::PARAM_STR);
        $stmt->bindParam(':cuisine',$cuisine,PDO::PARAM_STR);
        $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
        $stmt->execute();

        // $return["response"] = "Success";
        // echo json_encode($return);
        // exit();

        $query = "SELECT r.restaurantid, r.name, street, building, city, phone, price, cuisine, grade, latitude, longitude, r.total_rating, ra.rating, l.time, cc.count as commentcount FROM restaurants r left join rating ra on ra.restaurantid = r.restaurantid left join likes l on l.restaurantid=r.restaurantid left join comment_count cc on cc.restaurantid=r.restaurantid  WHERE";

        if($cuisine != "" or $cuisine != null){
            $query .= " LOWER(cuisine) like LOWER('".$cuisine."') ";
        }

        $numItems = count($keys);
        $i = 0;
        foreach ($keys as $key => $value) {
            if($key == 0){
                if(trim($value) != ""){
                    $query .= " and (";
                    $query .= " LOWER(name) like LOWER('%".trim($value)."%') ";
                }
            }else{
                if(trim($value) != ""){
                    $query .= " or ";
                    $query .= " LOWER(name) like LOWER('%".trim($value)."%') ";
                }
            }
            if(++$i === $numItems) {
                if(trim($value) != ""){
                    $query .= " )";
                }
            }

        }

        $query .= "group by `name`, `street`, `building`, `city` having max(gradedate) order by gradedate desc";
        $return["query"] = $query;

        $stmt2 = $db->prepare($query);
        $stmt2->bindValue(':cuisine',$cuisine,PDO::PARAM_STR);

        try{
            $stmt2->execute();
            $result = $stmt2->fetchAll(PDO::FETCH_OBJ);
            //$result = $stmt2->fetchAll(PDO::FETCH_ASSOC);
            $outputArray = array();
            $outputArray['searchBox'] = $searchBox;
            $outputArray['cuisine'] = $cuisine;
            $outputArray["results"] = $result;
            showSuccess($outputArray);
            $db = null;
            exit();
        }
        catch(PDOException $e){
            showError("100", $e->getMessage());
            $db = null;
            exit();
        }
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Search �  Additional Search for Restaurant/Cuisine - End

/// Add Comment  on Restaurants - Begin

function comment_submit(){

    include_once("check_session.php");

    $comment = strip_tags($_POST['comment_text']);
    $comment = trim($comment);
    $restaurantid = strip_tags($_POST['restaurant_id']);
    $syncid = $_POST['syncid'];
    try{
        $stmt = $db->prepare("INSERT INTO comments (restaurantid, comment_text, syncid, time)
        VALUES ( :restaurantid, :comment, :syncid , now())");
        $stmt->bindParam(':restaurantid',$restaurantid,PDO::PARAM_STR);
        $stmt->bindParam(':comment',$comment,PDO::PARAM_STR);
        $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
        $stmt->execute();
        $outputArray = array();
        $outputArray['comment_text'] = $comment_text;
        $outputArray['restaurant_id'] = $restaurant_id;
        $outputArray["response"] = "Comment added successfuly";
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Add Comment  on Restaurants - End

/// Fetch Comments  for a Restaurant - Begin
function fetch_restaurant_comments(){

    include_once("check_session.php");
    $restaurantid = strip_tags($_POST['restaurant_id']);
    try{
        $stmt = $db->prepare("SELECT c.commentid, c.restaurantid, c.comment_text, c.syncid, c.time, s.name, s.path FROM comments c join profile s on c.syncid=s.syncid WHERE restaurantid = :restaurantid");
        $stmt->bindParam(':restaurantid',$restaurantid,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray['restaurant_id'] = $restaurant_id;
        $outputArray["result"] = $result;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Fetch Comments  for a Restaurant - End

/// Delete Comment  on Restaurants - Begin

function delete_comment(){

    include_once("check_session.php");
    $syncid = $_POST['syncid'];
    $commentid = strip_tags($_POST['commentid']);
    try{
        $stmt = $db->prepare("DELETE FROM comments WHERE commentid=:commentid AND syncid=:syncid LIMIT 1");
        $stmt->bindParam(':commentid',$commentid,PDO::PARAM_STR);
        $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
        $stmt->execute();
        $db->commit();
        $outputArray = array();
        $outputArray['commentid'] = $commentid;
        $outputArray["response"] = "Comment deleted successfuly";
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Delete Comment  on Restaurants - End

/// Like a Restaurant - Begin

function like_restaurant(){

    include_once("check_session.php");
    $restaurantid = strip_tags($_POST['restaurant_id']);
    $syncid = $_POST['syncid'];
    try{
        $stmt = $db->prepare("INSERT INTO likes (restaurantid, syncid, time)
        VALUES ( :restaurantid, :syncid , now())");
        $stmt->bindParam(':restaurantid',$restaurantid,PDO::PARAM_STR);
        $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
        $stmt->execute();
        $outputArray = array();
        $outputArray['restaurant_id'] = $restaurant_id;
        $outputArray['syncid'] = $syncid;
        $outputArray["response"] = "Add like successful";
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Like a Restaurant - End

/// Delete Like on a Restaurant - Begin

function unlike_restaurant(){

    include_once("check_session.php");
    $restaurantid = strip_tags($_POST['restaurant_id']);
    $syncid = $_POST['syncid'];
    try{
        $stmt = $db->prepare("DELETE FROM likes WHERE restaurantid=:restaurantid AND syncid=:syncid LIMIT 1");
        $stmt->bindParam(':restaurantid',$restaurantid,PDO::PARAM_STR);
        $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
        $stmt->execute();
        $outputArray = array();
        $outputArray['restaurant_id'] = $restaurant_id;
        $outputArray['syncid'] = $syncid;
        $outputArray["response"] = "Delete like successful";
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Delete Like on a Restaurant - End

/// Fetch  list of users who have liked a Restaurant - Begin

function fetch_restaurant_likes(){

    include_once("check_session.php");
    $restaurantid = strip_tags($_POST['restaurant_id']);
    try{
        $stmt = $db->prepare("SELECT l.syncid, s.name FROM likes l join synclogin s on l.syncid=s.syncid WHERE l.restaurantid = :restaurantid");
        $stmt->bindParam(':restaurantid',$restaurantid,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray['restaurant_id'] = $restaurantid;
        $outputArray["result"] = $result;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Fetch  list of users who have liked a Restaurant - End

/// Fetch  list of Restaurants liked by a User - Begin


function fetch_usr_rstrnt_likes(){
    include_once("check_session.php");
    $syncid = strip_tags($_POST['syncid']);
    try{
        $stmt = $db->prepare("SELECT s.* FROM likes l join restaurants s on l.restaurantid=s.restaurantid WHERE l.syncid=:syncid order by l.time desc");
        $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray['syncid'] = $syncid;
        $outputArray["result"] = $result;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Fetch  list of Restaurants liked by a User - End

/// Counts Likes on a Restaurant - Begin

function count_restaurant_likes(){
    include_once("check_session.php");
    $restaurantid = strip_tags($_POST['restaurantid']);
    try{
        $stmt = $db->prepare("SELECT count(*) as count FROM likes WHERE restaurantid = :restaurantid");
        $stmt->bindParam(':restaurantid',$restaurantid,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray['restaurantid'] = $restaurantid;
        $outputArray["result"] = $result;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Counts Likes on a Restaurant - End
/// Rate a Restaurant - Begin

function add_rating(){
    include_once("check_session.php");
    $restaurantid = strip_tags($_POST['restaurant_id']);
    $rating = strip_tags($_POST['rating']);
    $syncid = $_POST['syncid'];
    try{
        $stmt = $db->prepare("SELECT syncid FROM rating WHERE syncid=:syncid and restaurantid = :restaurantid LIMIT 1");
        $stmt->bindValue(':syncid',$syncid,PDO::PARAM_STR);
        $stmt->bindParam(':restaurantid',$restaurantid,PDO::PARAM_STR);
        $stmt->execute();
        $count = $stmt->rowCount();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
    if($count > 0){
        try{
            $stmt1 = $db->prepare("UPDATE rating SET rating=:rating WHERE syncid=:syncid and restaurantid = :restaurantid");
            $stmt1->bindParam(':restaurantid',$restaurantid,PDO::PARAM_STR);
            $stmt1->bindParam(':rating',$rating,PDO::PARAM_STR);
            $stmt1->bindParam(':syncid',$syncid,PDO::PARAM_STR);
            $stmt1->execute();
	        $outputArray = array();
	        $outputArray['restaurant_id'] = $restaurant_id;
	        $outputArray['rating'] = $rating;
	        $outputArray['syncid'] = $syncid;
	        $outputArray["response"] = "Update rating successful";
	        showSuccess($outputArray);
	        $db = null;
	        exit();
        }
        catch(PDOException $e){
	        showError("100", $e->getMessage());
	        $db = null;
	        exit();
        }
    }else{
        try{
            $stmt2 = $db->prepare("INSERT INTO rating (restaurantid, syncid, rating, time)
            VALUES ( :restaurantid, :syncid , :rating, now())");
            $stmt2->bindParam(':restaurantid',$restaurantid,PDO::PARAM_STR);
            $stmt2->bindParam(':rating',$rating,PDO::PARAM_STR);
            $stmt2->bindParam(':syncid',$syncid,PDO::PARAM_STR);
            $stmt2->execute();
	        $outputArray = array();
	        $outputArray['restaurant_id'] = $restaurant_id;
	        $outputArray['rating'] = $rating;
	        $outputArray['syncid'] = $syncid;
	        $outputArray["response"] = "Add rating successful";
	        showSuccess($outputArray);
	        $db = null;
	        exit();
        }
        catch(PDOException $e){
	        showError("100", $e->getMessage());
	        $db = null;
	        exit();
        }
    }
}

/// Rate a Restaurant - End

/// Get Avg Rating for a Restaurant - Begin

function get_avg_rating(){
    include_once("check_session.php");
    $restaurantid = strip_tags($_POST['restaurant_id']);
    try{
        $stmt = $db->prepare("SELECT avg(rating) as rating FROM rating WHERE restaurantid = :restaurantid");
        $stmt->bindParam(':restaurantid',$restaurantid,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray['restaurant_id'] = $restaurant_id;
        $outputArray["result"] = $result;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Get Avg Rating for a Restaurant - End

/// Get Popular Restaurants - Begin

function get_popular_restaurant(){
    include_once("check_session.php");
    try{
        $stmt = $db->prepare("SELECT r.restaurantid, r.name, r.street, r.building,r.city,r.state,r.zip,r.total_rating,r.grade,r.count as like_cnt from restaurants r order by r.count desc,r.total_rating desc limit 200");
        $stmt->execute();
        $count = $stmt->rowCount();
        if($count > 0){
            $result = $stmt->fetchAll(PDO::FETCH_OBJ);
	        $outputArray = array();
	        $outputArray["result"] = $result;
	        showSuccess($outputArray);
	        $db = null;
	        exit();
        }else{
	        $outputArray = array();
	        $outputArray["result"] = "List is empty";
	        showSuccess($outputArray);
	        $db = null;
	        exit();
        }
    }
    catch(Exception $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Get Popular Restaurants - End

/// Upload a dish - begin

function upload_dish(){
  include_once("check_session.php");
  if (isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
      $restaurantid = strip_tags($_POST['restaurant_id']);
      $rating = strip_tags($_POST['rating']);
      $title = strip_tags($_POST['title']);
      $description = strip_tags($_POST['description']);
      $name = strip_tags($_POST['dish']);
      $public = strip_tags($_POST['public']);
      $syncid = $_POST['syncid'];
      $image = addslashes(file_get_contents($_FILES['image']['tmp_name'])); //SQL Injection defence!
      $image_name = addslashes($_FILES['image']['name']);
      $image_size = getimagesize($_FILES['image']['tmp_name']);
      //Download and save to computer
      $name = $_FILES['image']['name'] ;
      $filename = './userdirectory/'.$syncid.'/dishes/'. $name;
      $img= file_get_contents($_FILES['image']['tmp_name']);
      file_put_contents($filename, $img);
      if($image_size==FALSE){
	    $outputArray = array();
	    $outputArray["response"] = "Please select an image";
	    showSuccess($outputArray);
	    $db = null;
	    exit();
      }else{
        try{
          $stmt = $db->prepare("INSERT INTO dishes (syncid,restaurantid,title,description,name,rating,time,public,path)
            VALUES ($syncid,$restaurantid,'$title','$description','$name',$rating,now(),'$public','$filename')");
          $stmt->execute();
			$outputArray = array();
			$outputArray["response"] = "Uploaded";
			showSuccess($outputArray);
			$db = null;
			exit();
        }catch(Exception $e){
	        showError("100", $e->getMessage());
	        $db = null;
	        exit();
        }
      }

  }else{
    $outputArray = array();
    $outputArray["response"] = "Please select an image";
    showSuccess($outputArray);
    $db = null;
    exit();
    }

}

/// Upload a dish - End

///Getting List of Restaurants - Begin

function get_rest_list(){
    include_once("check_session.php");
    $keyword = strip_tags($_POST['restaurantName']);
    $keyword = '%'.$keyword.'%';
    try{
        $stmt = $db->prepare("SELECT * FROM restaurant_list where name like :keyword LIMIT 200");
        $stmt->bindValue(':keyword',$keyword,PDO::PARAM_STR);
        $stmt->execute();
        $count = $stmt->rowCount();
        if($count > 0){
            $result = $stmt->fetchAll(PDO::FETCH_OBJ);
	        $outputArray = array();
	        $outputArray['restaurantName'] = strip_tags($_POST['restaurantName']);
	        $outputArray["result"] = $result;
	        showSuccess($outputArray);
	        $db = null;
	        exit();
        }else{
	        $outputArray = array();
	        $outputArray['restaurantName'] = strip_tags($_POST['restaurantName']);
	        $outputArray["result"] = "No results found!";
	        showSuccess($outputArray);
	        $db = null;
	        exit();
        }
    }
    catch(Exception $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

///Getting List of Restaurants - End

/// Getting Dishes Uploaded by User - Begin

function get_dish_user(){
	$return = $_POST;
	include_once("check_session.php");
	$syncid = strip_tags($_POST['syncid']);
	$syncid = trim($syncid);
	try{
	    $stmt = $db->prepare("SELECT d.*, COALESCE((SELECT IF(dlc.syncid=:syncid,1,0) FROM dish_like_cnt dlc WHERE dlc.dishid = d.dishid AND dlc.syncid=:syncid), 0) as user_like,
                                    COALESCE((SELECT IF(ddc.syncid=:syncid,1,0) FROM dish_dislike_cnt ddc WHERE ddc.dishid = d.dishid AND ddc.syncid=:syncid), 0) as user_dislike
                                FROM dish_details d WHERE d.syncid = :syncid ORDER BY time DESC");
	    $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
	    $stmt->execute();
	    $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray['userid'] = $userid;
        $outputArray["result"] = $result;
        showSuccess($outputArray);
        $db = null;
        exit();
	}
	catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
	}
}

/// Getting Dishes Uploaded by User - End

/// Get Dishes Uploaded for a Restaurant - Begin

function get_dish_restaurant(){
    include_once("check_session.php");
    $restaurantid = strip_tags($_POST['restaurant_id']);
    try{
        $stmt = $db->prepare("SELECT * FROM dish_details WHERE restaurantid = :restaurantid AND public = '1'");
        $stmt->bindParam(':restaurantid',$restaurantid,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray['restaurant_id'] = $restaurant_id;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Get Dishes Uploaded for a Restaurant - Begin

/// Get All Dishes Uploaded by a User for a Restaurant - Begin

function get_dish_usr_restaurant(){
    include_once("check_session.php");
    $restaurantid = strip_tags($_POST['restaurant_id']);
    $userid = strip_tags($_POST['userid']);
    $userid = trim($userid);
    try{
        $stmt = $db->prepare("SELECT * FROM dish_details WHERE restaurantid = :restaurantid and syncid=:syncid");
        $stmt->bindParam(':restaurantid',$restaurantid,PDO::PARAM_STR);
        $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray['restaurant_id'] = $restaurant_id;
        $outputArray['userid'] = $userid;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Get All Dishes Uploaded by a User for a Restaurant - End

/// Get All Dishes Uploaded Users - Begin

function get_dish_all(){
    include_once("check_session.php");
    try{
        $stmt = $db->prepare("SELECT * FROM dish_details WHERE public = '1' ORDER BY dish_details.time DESC LIMIT 50");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray["result"] = $result;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Get All Dishes Uploaded Users - End

/// Get Popular Dishes - Begin

function get_popular_dish(){
    include_once("check_session.php");
    $syncid = $_POST['syncid'];
    try{
        $stmt = $db->prepare("SELECT `d`.`dishid` AS `dishid`,`d`.`syncid` AS `syncid`,`d`.`username` AS `username`,`d`.`profile_pic_path` AS `profile_pic_path`,`d`.`restaurantid` AS `restaurantid`,`d`.`dish_pic` AS `dish_pic`,`d`.`approved` AS `approved`,`d`.`public` AS `public`,`d`.`restname` AS `restname`,`d`.`restrating` AS `restrating`,`d`.`title` AS `title`,`d`.`description` AS `description`,`d`.`name` AS `name`,`d`.`rating` AS `rating`,`d`.`time` AS `time`,`d`.`street` AS `street`,`d`.`building` AS `building`,`d`.`city` AS `city`,`d`.`state` AS `state`,`d`.`zip` AS `zip`,`d`.`latitude` AS `latitude`,`d`.`longitude` AS `longitude`,`d`.`price` AS `price`,`d`.`grade` AS `grade`,`d`.`phone` AS `phone`,`d`.`comment_count` AS `comment_count`,`d`.`like_count` AS `like_count`,`d`.`dislike_count` AS `dislike_count`, SUM(`dr`.`score`) AS `score`, `d`.`like_syncid` AS `like_syncid`,
                                COALESCE((SELECT IF(dlc.syncid=:syncid,1,0) FROM dish_like_cnt dlc WHERE dlc.dishid = d.dishid AND dlc.syncid=:syncid), 0) as user_like,
                                COALESCE((SELECT IF(ddc.syncid=:syncid,1,0) FROM dish_dislike_cnt ddc WHERE ddc.dishid = d.dishid AND ddc.syncid=:syncid), 0) as user_dislike
                                FROM (`dish_details` `d`
                                LEFT JOIN `dish_rating` `dr` ON((`d`.`dishid` = `dr`.`dishid`)))
                                GROUP BY `d`.`dishid`
                                HAVING ((`d`.`public` = '1') AND (`d`.`approved` = '1'))
                                ORDER BY SUM(`dr`.`score`) DESC LIMIT 50");

        $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);

        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray["result"] = $result;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Get Popular Dishes - End

// Get Dish Uploads by other Users you Follow - Begin

function get_follow_dish(){

    include_once("check_session.php");
    $syncid = $_POST['syncid'];
    try{
        $stmt = $db->prepare("select d.*,
            COALESCE((SELECT IF(dlc.syncid=:syncid,1,0) FROM dish_like_cnt dlc WHERE dlc.dishid = d.dishid AND dlc.syncid=:syncid), 0) as user_like,
            COALESCE((SELECT IF(ddc.syncid=:syncid,1,0) FROM dish_dislike_cnt ddc WHERE ddc.dishid = d.dishid AND ddc.syncid=:syncid), 0) as user_dislike
            from following f left join dish_details d on f.follow_syncid=d.syncid where d.dishid is not null and f.syncid=:syncid ORDER BY d.time DESC LIMIT 50");
        $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray['syncid'] = $syncid;
        $outputArray["result"] = $result;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }

}

/// Get Dish Uploads by other Users you Follow  - End

/// Like a Dish - Begin

function like_dish(){
    include_once("check_session.php");
    $dishid = strip_tags($_POST['dishid']);
    $syncid = $_POST['syncid'];
    try{
        $stmt1 = $db->prepare("SELECT syncid FROM dish_like_cnt WHERE syncid=:syncid and dishid=:dishid");
        $stmt1->bindValue(':syncid',$syncid,PDO::PARAM_STR);
        $stmt1->bindParam(':dishid',$dishid,PDO::PARAM_STR);

        try{
          $stmt1->execute();
          $count = $stmt1->rowCount();

          if($count > 0){
//            $stmt = $db->prepare("delete from dish_like_cnt WHERE syncid=:syncid and dishid=:dishid ");
//            $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
//            $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
//			$stmt->execute();

//			$stmt = $db->prepare("delete from dish_rating WHERE syncid=:syncid and dishid=:dishid and score = 1");
//            $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
//            $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
//            $stmt->execute();
            $outputArray = array();
	        $outputArray['response'] = "You had Already Liked it";
	        $outputArray['syncid'] = $syncid;
	        $outputArray['dishid'] = $dishid;
	        showSuccess($outputArray);

          }else{
            $stmt = $db->prepare("INSERT INTO dish_like_cnt (dishid, syncid, like_count)
            VALUES ( :dishid, :syncid ,1)");
            $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
            $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
            $stmt->execute();

            $stmt = $db->prepare("INSERT IGNORE INTO dish_rating (dishid, syncid, time, score)
            VALUES ( :dishid, :syncid , now(), 1)");
            $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
            $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
            $stmt->execute();


            $stmt = $db->prepare("DELETE FROM dish_dislike_cnt WHERE dishid=:dishid AND syncid=:syncid");
            $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
            $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
            $stmt->execute();

            $stmt = $db->prepare("DELETE FROM dish_rating WHERE dishid = :dishid AND syncid = :syncid AND score = -1");
            $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
            $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
            $stmt->execute();


            $outputArray = array();
	        $outputArray['response'] = "successful";
	        $outputArray['syncid'] = $syncid;
	        $outputArray['dishid'] = $dishid;
	        showSuccess($outputArray);

            }

	        $db = null;
	        exit();
            }

        catch(Exception $e){
	        showError("100", $e->getMessage());
	        $db = null;
	        exit();
      }
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }

}

/// Like a Dish - End

/// Dislike a Dish  - Beign

function unlike_dish(){
    include_once("check_session.php");
    $dishid = strip_tags($_POST['dishid']);
    $syncid = $_POST['syncid'];
    try{
        $stmt1 = $db->prepare("SELECT syncid FROM dish_dislike_cnt WHERE syncid=:syncid and dishid=:dishid LIMIT 1");
        $stmt1->bindValue(':syncid',$syncid,PDO::PARAM_STR);
        $stmt1->bindParam(':dishid',$dishid,PDO::PARAM_STR);

        try{
          $stmt1->execute();
          $count = $stmt1->rowCount();


          if($count > 0){
//            $stmt = $db->prepare("delete from dish_dislike_cnt WHERE syncid=:syncid and dishid=:dishid ");
//            $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
//            $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);

//            $stmt->execute();

//            $stmt = $db->prepare("delete from dish_rating WHERE syncid=:syncid and dishid=:dishid and score = -1");
//            $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
//            $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
//            $stmt->execute();
            $outputArray = array();
	        $outputArray['response'] = "You had Already DisLiked";
	        $outputArray['syncid'] = $syncid;
	        $outputArray['dishid'] = $dishid;
	        showSuccess($outputArray);

          }else{
            $stmt = $db->prepare("INSERT INTO dish_dislike_cnt (dishid, syncid, dislike_count)
            VALUES ( :dishid, :syncid ,1)");
            $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
            $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
            $stmt->execute();

            $stmt = $db->prepare("delete from dish_like_cnt WHERE syncid=:syncid and dishid=:dishid ");
            $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
            $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);

            $stmt->execute();
            //echo '<pre>';print_r($stmt);die;
            $stmt = $db->prepare("delete from dish_rating WHERE syncid=:syncid and dishid=:dishid AND score = 1 ");
            $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
            $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);

            $stmt->execute();


			$stmt = $db->prepare("INSERT INTO dish_rating (dishid, syncid, time, score)
            VALUES ( :dishid, :syncid , now(), -1)");
            $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
            $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
            $stmt->execute();

            $outputArray = array();
	        $outputArray['response'] = "successful";
	        $outputArray['syncid'] = $syncid;
	        $outputArray['dishid'] = $dishid;
	        showSuccess($outputArray);

            }

	        $db = null;
	        exit();
            }

        catch(Exception $e){
	        showError("100", $e->getMessage());
	        $db = null;
	        exit();
      }
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }

}

/// Dislike a Dish - End

/// Remove Like/Dislike on a Dish  - Begin

function del_like_dish(){
    include_once("check_session.php");
    $dishid = strip_tags($_POST['dishid']);
    $syncid = $_POST['syncid'];
    try{
        $stmt = $db->prepare("DELETE FROM dish_rating WHERE dishid = :dishid AND syncid = :syncid");
        $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
        $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
        $outputArray = array();
        $outputArray['response'] = "successful";
        $outputArray['syncid'] = $syncid;
        $outputArray['dishid'] = $dishid;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }

}

/// Remove Like/Dislike on a Dish  - End

/// Fetch All Dishes Liked by a User - Begin

function fetch_usr_dish_likes(){
    include_once("check_session.php");
    $syncid = $_POST['syncid'];
    try{
        $stmt = $db->prepare("SELECT dr.syncid,dd.* FROM dish_rating dr left join dish_details dd on dr.dishid=dd.dishid where dr.score=1 and dr.syncid=:syncid LIMIT 50");
        $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray['result'] = $result;
        $outputArray['syncid'] = $syncid;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Fetch All Dishes Liked by a User - End

/// Comment on a Dish - Begin

function dish_comment_submit(){
    include_once("check_session.php");
    $comment = strip_tags($_POST['comment_text']);
    $comment = trim($comment);
    $dishid = $_POST['dishid'];
    $syncid = $_POST['syncid'];
    try{
        $stmt = $db->prepare("INSERT INTO dish_comment (dishid, comment_text, syncid, time)
        VALUES ( :dishid, :comment, :syncid , now())");
        $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
        $stmt->bindParam(':comment',$comment,PDO::PARAM_STR);
        $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
        $stmt->execute();
        $outputArray = array();
        $outputArray['comment_text'] = $comment_text;
        $outputArray['dishid'] = $dishid;
        $outputArray['syncid'] = $syncid;
        $outputArray['response'] = "successful";
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}
/// Comment on a Dish - End

/// Fetch List of Comment on a Dish  - Begin

function fetch_dish_comments(){
    include_once("check_session.php");
    $dishid = strip_tags($_POST['dishid']);
    try{
        $stmt = $db->prepare("SELECT c.commentid, c.dishid, c.comment_text, c.syncid, c.time, s.name, s.path FROM dish_comment c join profile s on c.syncid=s.syncid WHERE c.dishid = :dishid");
        $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray["result"] = $result;
        $outputArray['dishid'] = $dishid;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Fetch List of Comment on a Dish  - End

/// Fetch List of User Comment on a Dish - Begin

function fetch_user_dish_comments(){
    include_once("check_session.php");
    $syncid = $_POST['syncid'];
    $dishid = strip_tags($_POST['dishid']);
    try{
        $stmt = $db->prepare("SELECT commentid, c.dishid, comment_text, c.syncid, c.time, r.* FROM dish_comment c join dishes r on c.dishid=r.dishid WHERE c.syncid = :syncid AND c.dishid = :dishid ");
        $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
        $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray["result"] = $result;
        $outputArray['dishid'] = $dishid;
        $outputArray['syncid'] = $syncid;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Fetch List of User Comment on a Dish - End

/// Delete Comment on a Dish - Begin

function delete_dish_comment(){
    include_once("check_session.php");
    $syncid = $_POST['syncid'];
    $commentid = strip_tags($_POST['commentid']);
    try{
        $stmt = $db->prepare("DELETE FROM dish_comment WHERE commentid=:commentid AND syncid=:syncid LIMIT 1");
        $stmt->bindParam(':commentid',$commentid,PDO::PARAM_STR);
        $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
        $stmt->execute();
        $db->commit();
        $outputArray = array();
        $outputArray["response"] = "successful";
        $outputArray['commentid'] = $commentid;
        $outputArray['syncid'] = $syncid;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Delete Comment on a Dish - End

/// Fetch List Users Who Commented on a Dish - Begin

function get_dish_comment_usr_list(){
    include_once("check_session.php");
    $dishid = strip_tags($_POST['dishid']);
    try{
        $stmt = $db->prepare("select d.*,p.name,p.path from d left join profile p on d.syncid=p.syncid where dishid = :dishid");
        $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray["result"] = $result;
        $outputArray['dishid'] = $dishid;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Fetch List Users Who Commented on a Dish - End

/// Fetch List Users Who Liked a Dish - Begin

function get_dish_like_usr_list(){
    include_once("check_session.php");
    $dishid = strip_tags($_POST['dishid']);
    try{
        $stmt = $db->prepare("select d.*,p.name,p.path from dish_rating d left join profile p on d.syncid=p.syncid where dishid = :dishid and score='1'");
        $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray["result"] = $result;
        $outputArray['dishid'] = $dishid;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Fetch List Users Who Liked a Dish - End

/// Fetch List Users Who Disliked a Dish - Begin

function get_dish_dislike_usr_list(){
    include_once("check_session.php");
    $dishid = strip_tags($_POST['dishid']);
    try{
        $stmt = $db->prepare("select d.*,p.name,p.path from dish_rating d left join profile p on d.syncid=p.syncid where dishid = :dishid and score='-1'");
        $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray["result"] = $result;
        $outputArray['dishid'] = $dishid;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Fetch List Users Who Disliked a Dish - End

/// Add Restaurant - Begin

function add_restaurant(){
	include_once("check_session.php");
	$syncid = $_POST['syncid'];
	$name = trim(strip_tags($_POST['name']));
	$street = trim(strip_tags($_POST['street']));
	$building = trim(strip_tags($_POST['building']));
	$city = trim(strip_tags($_POST['city']));
	$zip = trim(strip_tags($_POST['zip']));
	$price = trim(strip_tags($_POST['price']));
	$phone = trim(strip_tags($_POST['phone']));
	$cuisine = trim(strip_tags($_POST['cuisine']));
	$grade = trim(strip_tags($_POST['grade']));
	try{
	    $stmt = $db->prepare("INSERT INTO restaurants (name,street,building,city,zip,price,phone,cuisine,grade)
	    VALUES (:name,:street,:building,:city,:zip,:price,:phone,:cuisine,:grade) ");
	    $stmt->bindParam(':name',$name,PDO::PARAM_STR);
	    $stmt->bindParam(':street',$street,PDO::PARAM_STR);
	    $stmt->bindParam(':building',$building,PDO::PARAM_STR);
	    $stmt->bindParam(':city',$city,PDO::PARAM_STR);
	    $stmt->bindParam(':zip',$zip,PDO::PARAM_STR);
	    $stmt->bindParam(':price',$price,PDO::PARAM_STR);
	    $stmt->bindParam(':phone',$phone,PDO::PARAM_STR);
	    $stmt->bindParam(':cuisine',$cuisine,PDO::PARAM_STR);
	    $stmt->bindParam(':grade',$grade,PDO::PARAM_STR);
	    $stmt->execute();

	    $restaurantid = $db->lastInsertId();
	    $stmt1 = $db->prepare("INSERT INTO user_restaurant (syncid,restaurantid,time)
	    VALUES (:syncid,:restaurantid,now()) ");
	    $stmt1->bindParam(':syncid',$syncid,PDO::PARAM_STR);
	    $stmt1->bindParam(':restaurantid',$restaurantid,PDO::PARAM_STR);
	    $stmt1->execute();
        $outputArray = array();
        $outputArray["response"] = "successful";
        $outputArray['syncid'] = $syncid;
        $outputArray['name'] = $name;
        $outputArray['street'] = $street;
        $outputArray['building'] = $building;
        $outputArray['city'] = $city;
        $outputArray['zip'] = $zip;
        $outputArray['price'] = $price;
        $outputArray['phone'] = $phone;
        $outputArray['cuisine'] = $cuisine;
        $outputArray['grade'] = $grade;
        showSuccess($outputArray);
        $db = null;
        exit();
	}
	catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
	}
}

/// Add Restaurant - End

/// Upload Pic for a Restaurant - Begin

function upload_restaurant_pic(){
  include_once("check_session.php");
  if (isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
      $restaurantid = strip_tags($_POST['restaurant_id']);
      $description = strip_tags($_POST['description']);
      $syncid = $_POST['syncid'];
      $image = addslashes(file_get_contents($_FILES['image']['tmp_name'])); //SQL Injection defence!
      $image_name = addslashes($_FILES['image']['name']);
      $image_size = getimagesize($_FILES['image']['tmp_name']);
      //Download and save to computer
      $name = $_FILES['image']['name'] ;
      $filename = './userdirectory/'.$syncid.'/restaurants/'. $name;
      $img= file_get_contents($_FILES['image']['tmp_name']);
      file_put_contents($filename, $img);

      if($image_size==FALSE){
	    showError("8", "Please select an image");
	    $db = null;
	    exit();
      }else{
        try{
          $stmt = $db->prepare("INSERT INTO restaurant_pics (syncid,restaurantid,description,time,path)
            VALUES (:syncid,:restaurantid,:description,now(),:filename)");
          $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
          $stmt->bindParam(':restaurantid',$restaurantid,PDO::PARAM_STR);
          $stmt->bindParam(':description',$description,PDO::PARAM_STR);
          $stmt->bindParam(':filename',$filename,PDO::PARAM_STR);
          $stmt->execute();
			$outputArray = array();
			$outputArray["response"] = "successful";
			showSuccess($outputArray);
			$db = null;
			exit();
        }catch(Exception $e){
	        showError("100", $e->getMessage());
	        $db = null;
	        exit();
        }
      }
  }else{
    showError("8", "Please select an image");
    $db = null;
    exit();
    }

}
/// Upload Pic for a Restaurant - End

/// Add Dish to User List - Begin

function dish_add_to_list(){
    include_once("check_session.php");
    $dishid = strip_tags($_POST['dish_id']);
    $syncid = $_POST['syncid'];
    try{
        $stmt = $db->prepare("INSERT INTO following_dish (dishid, syncid, timestamp)
        VALUES ( :dishid, :syncid , now())");
        $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
        $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
        $stmt->execute();
        $outputArray = array();
        $outputArray["response"] = "successful";
        $outputArray['dish_id'] = $dishid;
        $outputArray['syncid'] = $syncid;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}
/// Add Dish to User List - End

/// Delete Dish from User List - Begin

function dish_delete_from_list(){
    include_once("check_session.php");
    $dishid = strip_tags($_POST['dish_id']);
    $syncid = $_POST['syncid'];
    try{
        $stmt = $db->prepare("DELETE FROM following_dish WHERE dishid=:dishid AND syncid=:syncid LIMIT 1");
        $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
        $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
        $stmt->execute();
        $outputArray = array();
        $outputArray["response"] = "successful";
        $outputArray['dish_id'] = $dishid;
        $outputArray['syncid'] = $syncid;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}
/// Delete Dish from User List - End

/// Fetch User�s List of Dishes - Begin

function fetch_user_dish_list(){
    include_once("check_session.php");
    $userid = trim(strip_tags($_POST['userid']));
    try{
        $stmt = $db->prepare("SELECT fd.syncid,dd.* FROM following_dish fd left join dish_details dd on fd.dishid=dd.dishid where fd.syncid=:syncid");
        $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray["result"] = $result;
        $outputArray['userid'] = $userid;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}
/// Fetch User�s List of Dishes - End

/// Save Name - Begin

function save_usr_name(){

  include_once("check_session.php");
  $username = strip_tags($_POST['username']);
  $syncid = $_POST['syncid'];

	try{
		$stmt1 = $db->prepare("UPDATE synclogin SET name=:username WHERE syncid=:syncid");
		$stmt1->bindParam(':username',$username);
		$stmt1->bindParam(':syncid',$syncid);
		$stmt1->execute();
		$stmt2 = $db->prepare("UPDATE signup SET name=:username WHERE signid=(select signid from synclogin where syncid=:syncid)");
		$stmt2->bindParam(':username',$username);
		$stmt2->bindParam(':syncid',$syncid);
		$stmt2->execute();
		$outputArray = array();
		$outputArray["username"] = $username;
		$outputArray['syncid'] = $syncid;
		showSuccess($outputArray);
		$db = null;
		exit();
	}
	catch(PDOException $e){
	    showError("100", $e->getMessage());
	    $db = null;
	    exit();
	}
}

/// Save Name - End

/// Upload Profile Pic - Begin

function upload_profile_pic(){
  include_once("check_session.php");
  if (isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
      $syncid = $_POST['syncid'];
      $image = addslashes(file_get_contents($_FILES['image']['tmp_name'])); //SQL Injection defence!
      $image_name = addslashes($_FILES['image']['name']);
      $image_size = getimagesize($_FILES['image']['tmp_name']);
      //Download and save to computer
      $name = $_FILES['image']['name'] ;
      $filename = './userdirectory/'.$syncid.'/profile/'. $name;
      $img= file_get_contents($_FILES['image']['tmp_name']);
      file_put_contents($filename, $img);

      if($image_size==FALSE){
        showError("8", "Please select an image");
        echo json_encode($return);
        $db = null;
        exit();
      }else{
        try{
            $stmt1 = $db->prepare("SELECT syncid FROM user_preference WHERE syncid=:syncid LIMIT 1");
            $stmt1->bindValue(':syncid',$syncid,PDO::PARAM_STR);
            try{
              $stmt1->execute();
              $count = $stmt1->rowCount();
              if($count > 0){
                  $stmt = $db->prepare("UPDATE user_preference SET path=:path WHERE syncid=:syncid");
                  $stmt->bindParam(':path',$filename,PDO::PARAM_STR);
                  $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
                  $stmt->execute();
					$outputArray = array();
					$outputArray["response"] = "uploaded";
					showSuccess($outputArray);
					$db = null;
					exit();
              }else{
                  $stmt = $db->prepare("INSERT INTO user_preference (syncid,path,time)
                    VALUES (:syncid,:path,now())");
                  $stmt->bindParam(':path',$filename,PDO::PARAM_STR);
                  $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
                  $stmt->execute();
					$outputArray = array();
					$outputArray["response"] = "uploaded";
					showSuccess($outputArray);
					$db = null;
					exit();
              }
            }catch(Exception $e){
			    showError("100", $e->getMessage());
			    $db = null;
			    exit();
        }

        }catch(Exception $e){
		    showError("100", $e->getMessage());
		    $db = null;
		    exit();
        }
      }

  }else{
    showError("8", "Please select an image");
    $db = null;
    exit();
    }
}


/// Upload Profile Pic - End

/// View User Profile - Begin

function user_profile(){
  include_once("check_session.php");
  $syncid = $_POST['syncid'];
  try{
      $stmt = $db->prepare("SELECT * FROM profile WHERE syncid = :syncid LIMIT 1");
      $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
      $stmt->execute();
      $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray["result"] = $result;
        $outputArray['syncid'] = $syncid;
        showSuccess($outputArray);
        $db = null;
        exit();
  }
  catch(PDOException $e){
    showError("100", $e->getMessage());
    $db = null;
    exit();
  }

}

/// View User Profile - End

/// Follow User - Begin

function follow_user(){
    include_once("check_session.php");
    $syncid = $_POST['syncid'];
    $userid = strip_tags($_POST['userid']);
    try{
        $stmt = $db->prepare("INSERT INTO following ( syncid, follow_syncid)
        VALUES ( :syncid, :userid )");
        $stmt->bindParam(':userid',$userid,PDO::PARAM_STR);
        $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
        $stmt->execute();
       	$outputArray = array();
        $outputArray["reponse"] = "success";
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
	    showError("100", $e->getMessage());
	    $db = null;
	    exit();
    }
}

/// Follow User - End

/// Unfollow User - Begin

function unfollow_user(){
    include_once("check_session.php");
    $syncid = $_POST['syncid'];
    $userid = strip_tags($_POST['userid']);
    try{
        $stmt = $db->prepare("DELETE FROM following WHERE follow_syncid=:userid AND syncid=:syncid");
        $stmt->bindParam(':userid',$userid,PDO::PARAM_STR);
        $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
        $stmt->execute();
       	$outputArray = array();
        $outputArray["reponse"] = "success";
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
	    showError("100", $e->getMessage());
	    $db = null;
	    exit();
    }
}

/// Unfollow User - End

/// View User Following - Begin

function view_following(){
  include_once("check_session.php");
   $userId = strip_tags($_POST['userid']);
   $syncid = strip_tags($_POST['syncid']);
  try{
      /*
      //$stmt = $db->prepare("SELECT p.* FROM following f left join profile p on f.follow_syncid=p.syncid where f.syncid=:syncid");
      $stmt = $db->prepare("SELECT follow_syncid FROM following WHERE syncid = :syncid");
      $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
      $stmt->execute();
      $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray["result"] = $result;
        $outputArray['syncid'] = $syncid;
        showSuccess($outputArray);
        $db = null;
        exit();
       *
       */

    $stmt2 = $db->prepare("SELECT follow_syncid FROM following WHERE syncid = :syncid AND follow_syncid = :userId");
    $stmt2->bindParam(':syncid',$syncid,PDO::PARAM_STR);
    $stmt2->bindParam(':userId',$userId,PDO::PARAM_STR);
    $stmt2->execute();

    $outputArray = array();
    $outputArray["result"] = ($stmt2->rowCount() > 0) ? "1" : "0";
    $outputArray['syncid'] = $syncid;
    $outputArray['userid'] = $userId;
    showSuccess($outputArray);

    $db = null;
    exit();
  }
  catch(PDOException $e){
    showError("100", $e->getMessage());
    $db = null;
    exit();
  }

}

/// View User Following - End

/// View User Followers - Begin

function view_followers(){
  include_once("check_session.php");
  $syncid = strip_tags($_POST['syncid']);
  try{
      //$stmt = $db->prepare("SELECT p.* FROM following f left join profile p on f.syncid=p.syncid where f.follow_syncid=:syncid");
      //$stmt = $db->prepare("SELECT p.* FROM following f JOIN profile p ON f.syncid=p.syncid WHERE f.follow_syncid=:userid");
      $stmt = $db->prepare("SELECT d.* FROM following f JOIN dish_details d ON f.syncid=d.syncid WHERE f.follow_syncid=:userid ORDER BY d.time DESC");
      $stmt->bindParam(':userid',$syncid,PDO::PARAM_STR);
      $stmt->execute();
      $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray["result"] = $result;
        $outputArray['syncid'] = $syncid;
        showSuccess($outputArray);
        $db = null;
        exit();
  }
  catch(PDOException $e){
    showError("100", $e->getMessage());
    $db = null;
    exit();
  }
}

/// View User Followers - End


function view_user_I_follow()
{
    include_once("check_session.php");
    $syncid = strip_tags($_POST['syncid']);
    try
    {
      $stmt = $db->prepare("SELECT p.* FROM profile AS p INNER JOIN following AS f ON f.follow_syncid = p.syncid WHERE f.syncid = :syncid ORDER BY p.name ASC");
      $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
      $stmt->execute();
      $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray["result"] = $result;
        $outputArray['syncid'] = $syncid;
        showSuccess($outputArray);
        $db = null;
        exit();
    } catch (Exception $e) {
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

function view_user_following_me()
{
    include_once("check_session.php");
    $syncid = strip_tags($_POST['syncid']);
    try
    {
      $stmt = $db->prepare("SELECT p.* FROM profile AS p INNER JOIN following AS f ON f.syncid = p.syncid WHERE f.follow_syncid = :syncid ORDER BY p.name ASC");
      $stmt->bindParam(':syncid',$syncid,PDO::PARAM_STR);
      $stmt->execute();
      $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray["result"] = $result;
        $outputArray['syncid'] = $syncid;
        showSuccess($outputArray);
        $db = null;
        exit();
    } catch (Exception $e) {
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

function cuisineList()
{
    include_once("check_session.php");
    try
    {
      $stmt = $db->prepare("SELECT cuisine FROM `restaurants` GROUP BY cuisine ORDER BY cuisine ASC");
      $stmt->execute();
      $finalResult = array();

        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                $finalResult[]= $row['cuisine'];
        }
        $outputArray = array();
        $outputArray["result"] = $finalResult;
        $outputArray['syncid'] = $syncid;
        showSuccess($outputArray);
        $db = null;
        exit();
    } catch (Exception $e) {
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}


function gradeList()
{
    include_once("check_session.php");
    try
    {
      $stmt = $db->prepare("SELECT  grade FROM  `restaurants` GROUP BY `grade`");
      $stmt->execute();
      $finalResult = array();

        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                $finalResult[]= $row['grade'];
        }
        $outputArray = array();
        $outputArray["result"] = $finalResult;
        $outputArray['syncid'] = $syncid;
        showSuccess($outputArray);
        $db = null;
        exit();
    } catch (Exception $e) {
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}


function count_dish_likes(){
    include_once("check_session.php");
    $dishid = strip_tags($_POST['dishid']);
    try{
        $stmt = $db->prepare("SELECT count(0) as cnt FROM dish_like_cnt WHERE dishid = :dishid");
        $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray['dishid'] = $dishid;
        $outputArray["result"] = $result;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}

/// Counts disLikes on a dish - Begin

function count_dish_dislikes(){
     include_once("check_session.php");
    $dishid = strip_tags($_POST['dishid']);
    try{
        $stmt = $db->prepare("SELECT count(0) as cnt FROM dish_dislike_cnt WHERE dishid = :dishid");
        $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray['dishid'] = $dishid;
        $outputArray["result"] = $result;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}


/// Counts comments on dish - Begin

function count_comment_dish(){
    include_once("check_session.php");
    $dishid = strip_tags($_POST['dishid']);
    try{
        $stmt = $db->prepare("SELECT count(*) as count FROM dish_comment WHERE dishid = :dishid");
        $stmt->bindParam(':dishid',$dishid,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray['dishid'] = $dishid;
        $outputArray["result"] = $result;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}


/// Counts comments on dish - End

/// Counts comments on restaurant - Begin

function count_comment_restaurant(){
    include_once("check_session.php");
    $restaurantid = strip_tags($_POST['restaurantid']);
    try{
        $stmt = $db->prepare("SELECT count FROM comment_count WHERE restaurantid = :restaurantid");
        $stmt->bindParam(':restaurantid',$restaurantid,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $outputArray = array();
        $outputArray['restaurantid'] = $restaurantid;
        $outputArray["result"] = $result;
        showSuccess($outputArray);
        $db = null;
        exit();
    }
    catch(PDOException $e){
        showError("100", $e->getMessage());
        $db = null;
        exit();
    }
}


function filter_search(){
    include_once("check_session.php");

    $keyword = strip_tags($_POST['price']);
    $keyword = trim($keyword);
    $cuisine = strip_tags($_POST['cuisine']);
    $cuisine = strtolower(trim($cuisine));
    $grade = $_POST['grade'];


    $strSql = "SELECT r.restaurantid, r.name, street, building, city, phone, price, cuisine, grade, latitude, longitude, r.total_rating, r.total_rating, r.count FROM restaurants r ";
    $strWhere = "";
    $strLimit = "LIMIT 50";


       try{
		if($cuisine != ""){
		if($keyword == "" && $grade == ""){
		$stmt2 = $db->prepare("SELECT r.restaurantid, r.name, street, building, city, phone, price, cuisine, grade, latitude, longitude, r.total_rating, r.total_rating, r.count FROM restaurants r WHERE cuisine=:cuisine LIMIT 50");
			$stmt2->bindValue(':cuisine',$cuisine,PDO::PARAM_STR);
		}
		else if($keyword != "" && $grade == ""){
		$stmt2 = $db->prepare("SELECT r.restaurantid, r.name, street, building, city, phone, price, cuisine, grade, latitude, longitude, r.total_rating, r.total_rating, r.count FROM restaurants r WHERE cuisine=:cuisine AND price=:price LIMIT 50");
			$stmt2->bindValue(':cuisine',$cuisine,PDO::PARAM_STR);
			$stmt2->bindValue(':price',$keyword,PDO::PARAM_STR);
		}
		else if($grade != "" && $keyword == ""){
		$stmt2 = $db->prepare("SELECT r.restaurantid, r.name, street, building, city, phone, price, cuisine, grade, latitude, longitude, r.total_rating, r.total_rating, r.count FROM restaurants r WHERE cuisine=:cuisine AND grade=:grade LIMIT 50");
			$stmt2->bindValue(':cuisine',$cuisine,PDO::PARAM_STR);
			$stmt2->bindValue(':grade',$grade,PDO::PARAM_STR);
		}
		else{
			$stmt2 = $db->prepare("SELECT r.restaurantid, r.name, street, building, city, phone, price, cuisine, grade, latitude, longitude, r.total_rating, r.total_rating, r.count FROM restaurants r WHERE cuisine=:cuisine AND price=:price AND grade=:grade LIMIT 50");
			$stmt2->bindValue(':cuisine',$cuisine,PDO::PARAM_STR);
			$stmt2->bindValue(':price',$keyword,PDO::PARAM_STR);
			$stmt2->bindValue(':grade',$grade,PDO::PARAM_STR);
			}
		}
		else{
			if($keyword != "" && $grade == ""){
			$stmt2 = $db->prepare("SELECT r.restaurantid, r.name, street, building, city, phone, price, cuisine, grade, latitude, longitude, r.total_rating, r.total_rating, r.count FROM restaurants r WHERE price=:price LIMIT 200");
			$stmt2->bindValue(':price',$keyword,PDO::PARAM_STR);
			}
			else if($grade != "" && $keyword == ""){
			$stmt2 = $db->prepare("SELECT r.restaurantid, r.name, street, building, city, phone, price, cuisine, grade, latitude, longitude, r.total_rating, r.total_rating, r.count FROM restaurants r WHERE grade=:grade LIMIT 200");
			$stmt2->bindValue(':grade',$grade,PDO::PARAM_STR);
			}
			else{
			$stmt2 = $db->prepare("SELECT r.restaurantid, r.name, street, building, city, phone, price, cuisine, grade, latitude, longitude, r.total_rating, r.total_rating, r.count FROM restaurants r WHERE grade=:grade AND price=:price LIMIT 200");
			$stmt2->bindValue(':grade',$grade,PDO::PARAM_STR);
			$stmt2->bindValue(':price',$keyword,PDO::PARAM_STR);
			}
			}




            // $stmt2->bindValue(':start',$start,PDO::PARAM_INT);
            // $stmt2->bindValue(':end',$end,PDO::PARAM_INT);
            try{
                $stmt2->execute();
                $result = $stmt2->fetchAll(PDO::FETCH_OBJ);
                //$result = $stmt2->fetchAll(PDO::FETCH_ASSOC);
                $outputArray = array();
                $outputArray['price'] = $keyword;
                $outputArray['cuisine'] = $cuisine;
				$outputArray['grade'] = $grade;
                $outputArray["results"] = $result;
                showSuccess($outputArray);
                $db = null;
                exit();
            }
            catch(PDOException $e){
                showError("100", $e->getMessage());
                $db = null;
                exit();
            }


        }
        catch(PDOException $e){
            showError("100", $e->getMessage());
            $db = null;
            exit();
        }

        $return["value"] =$outputArray;
        echo json_encode($return);
        $db = null;
        exit();
    }


	//Filter search with distance
function filter_search_distance(){
    include_once("check_session.php");

    $keyword = strip_tags($_POST['price']);
    $keyword = trim($keyword);
    $cuisine = strip_tags($_POST['cuisine']);
    $cuisine = strtolower(trim($cuisine));
    $grade = $_POST['grade'];
    $mood = $_POST['mood'];
	$distance = $_POST['distance'];
	$latitude = $_POST['latitude'];
	$longitude = $_POST['longitude'];
    $searchQuery = trim($_POST['searchBox']);
    $strCuisineOrder = '';

     if (is_numeric($searchQuery) && strlen($searchQuery) == 5)
     {
        $strGetZip = $db->prepare("SELECT z.lat, z.long FROM zipcode z WHERE zip=:zipcode LIMIT 1");
        $strGetZip->bindValue(':zipcode',$searchQuery,PDO::PARAM_STR);

        $strGetZip->execute();
        $zipCount = $strGetZip->rowCount();

        if($zipCount > 0)
        {
            while($row = $strGetZip->fetch(PDO::FETCH_ASSOC))
            {
                $latitude = $row['lat'];
                $longitude = $row['long'];
            }
            if( '' != trim($distance) )
            {
                $searchQuery = '';
            }

        }
     }

    $strSql = "SELECT r.zip, r.restaurantid, r.name, street, building, city, phone, price, r.cuisine, grade, latitude, longitude, r.total_rating, r.total_rating, r.count, ( 3959 * acos( cos( radians(:latitude) ) * cos( radians(latitude) ) * cos( radians(longitude) - radians(:longitude) ) + sin( radians(:latitude) ) * sin( radians(latitude) ) ) ) AS distance FROM restaurants r ";
    $strWhere = " WHERE 1=1 ";
       try{
        if( false == empty($searchQuery) ) {
            $strWhere .= " AND ( CAST( r.zip AS CHAR) LIKE '" . $searchQuery . "%' OR r.name LIKE '%". $searchQuery ."%' )";
        }
        if( '' != trim($mood) ) {
            $stmtMood = $db->prepare("SELECT cuisine FROM moods_cuisine WHERE mood=:mood LIMIT 1");
            $stmtMood->bindValue(':mood',$mood,PDO::PARAM_STR);

            $stmtMood->execute();
            $moodCount = $stmtMood->rowCount();

            if($moodCount > 0) {
                while($row = $stmtMood->fetch(PDO::FETCH_ASSOC)) {
                    if('' != trim($row['cuisine'])){
                        $stmtMood1 = $db->prepare("DROP TABLE IF EXISTS tmp_cuisine");
                        $stmtMood1->execute();
                        $stmtMood1 = $db->prepare("CREATE TABLE IF NOT EXISTS tmp_cuisine (id INT(10) AUTO_INCREMENT PRIMARY KEY, cuisine VARCHAR(255))");
                        $stmtMood1->execute();
                        $arrMoodSql = array();
                        foreach(explode("$", $row['cuisine']) as $strMood) {
                            $arrMoodSql[] = "('" . $strMood . "')";
                        }
                        $stmtMood1 = $db->prepare("INSERT INTO tmp_cuisine(`cuisine`) VALUES " . implode(", ", $arrMoodSql));
                        $stmtMood1->execute();
                        $strSql .= " JOIN tmp_cuisine tmp ON tmp.cuisine = r.cuisine ";
                        $cuisine = '';
                        $strCuisineOrder = " tmp.id ASC, ";
                    }
                }
            }
        }
        if( '' != trim($cuisine) ) {
            $strWhere .= " AND cuisine IN ('" . trim($cuisine) . "')";
        }
        if( '' != trim($keyword) ) {
            $strWhere .= " AND price = '" . trim($keyword) . "'";
        }
        if( '' != trim($grade) ) {
            $strWhere .= " AND grade = '" . trim($grade) . "'";
        }

        if( '' != trim($distance) ) {
            $strWhere .= " HAVING distance < :distance ";
        }

        $strLimit = " ORDER BY " . $strCuisineOrder . " distance LIMIT 200;";

//        $outputArray = array('sql'=>$strSql . $strWhere . $strLimit, 'data'=>$_REQUEST);
//
//        showSuccess($outputArray);exit;
//
        $stmt2 = $db->prepare( $strSql . $strWhere . $strLimit );
        $stmt2->bindValue(':longitude',$longitude,PDO::PARAM_STR);
	    $stmt2->bindValue(':latitude',$latitude,PDO::PARAM_STR);
        if( '' != trim($distance) ) {
            $stmt2->bindValue(':distance',$distance,PDO::PARAM_STR);
        }

		//if($cuisine != ""){
//		  if($distance != ""){
//		      if($keyword == "" && $grade == ""){
//		          $stmt2 = $db->prepare( $strSql . " HAVING distance < :distance and cuisine = :cuisine ORDER BY distance LIMIT 200;");
//                  $stmt2->bindValue(':cuisine',$cuisine,PDO::PARAM_STR);
//    			  $stmt2->bindValue(':distance',$distance,PDO::PARAM_STR);
//    			  $stmt2->bindValue(':longitude',$longitude,PDO::PARAM_STR);
//    			 $stmt2->bindValue(':latitude',$latitude,PDO::PARAM_STR);
//		      }else if($keyword != "" && $grade == ""){
//		          $stmt2 = $db->prepare($strSql . " HAVING distance < :distance and cuisine = :cuisine and price=:price ");
//			     $stmt2->bindValue(':cuisine',$cuisine,PDO::PARAM_STR);
//                 $stmt2->bindValue(':price',$keyword,PDO::PARAM_STR);
//			     $stmt2->bindValue(':distance',$distance,PDO::PARAM_STR);
//    			 $stmt2->bindValue(':longitude',$longitude,PDO::PARAM_STR);
//			     $stmt2->bindValue(':latitude',$latitude,PDO::PARAM_STR);
//		      }else if($grade != "" && $keyword == ""){
//		          $stmt2 = $db->prepare( $strSql . " HAVING distance < :distance and cuisine = :cuisine and grade = :grade ORDER BY distance LIMIT 200; ");
//			     $stmt2->bindValue(':cuisine',$cuisine,PDO::PARAM_STR);
//			     $stmt2->bindValue(':grade',$grade,PDO::PARAM_STR);
//			     $stmt2->bindValue(':distance',$distance,PDO::PARAM_STR);
//			     $stmt2->bindValue(':longitude',$longitude,PDO::PARAM_STR);
//			     $stmt2->bindValue(':latitude',$latitude,PDO::PARAM_STR);
//		      }else{
//        			$stmt2 = $db->prepare( $strSql . " HAVING distance < :distance and cuisine = :cuisine and grade = :grade and price=:price ORDER BY distance LIMIT 200; ");
//        			$stmt2->bindValue(':cuisine',$cuisine,PDO::PARAM_STR);
//                    $stmt2->bindValue(':price',$keyword,PDO::PARAM_STR);
//        			$stmt2->bindValue(':grade',$grade,PDO::PARAM_STR);
//        			$stmt2->bindValue(':distance',$distance,PDO::PARAM_STR);
//        			$stmt2->bindValue(':longitude',$longitude,PDO::PARAM_STR);
//        			$stmt2->bindValue(':latitude',$latitude,PDO::PARAM_STR);
//			 }
//		}else{
//			if($keyword == "" && $grade == ""){
//		          $stmt2 = $db->prepare( $strSql . " HAVING cuisine = :cuisine ORDER BY distance LIMIT 200;");
//			     $stmt2->bindValue(':cuisine',$cuisine,PDO::PARAM_STR);
//                 $stmt2->bindValue(':longitude',$longitude,PDO::PARAM_STR);
//			     $stmt2->bindValue(':latitude',$latitude,PDO::PARAM_STR);
//            }else if($keyword != "" && $grade == ""){
//                $stmt2 = $db->prepare( $strSql . " HAVING cuisine = :cuisine and price=:price ORDER BY distance LIMIT 200; ");
//    			$stmt2->bindValue(':cuisine',$cuisine,PDO::PARAM_STR);
//                $stmt2->bindValue(':price',$keyword,PDO::PARAM_STR);
//    			$stmt2->bindValue(':longitude',$longitude,PDO::PARAM_STR);
//    			$stmt2->bindValue(':latitude',$latitude,PDO::PARAM_STR);
//            }else if($grade != "" && $keyword == ""){
//                $stmt2 = $db->prepare( $strSql . " HAVING cuisine = :cuisine and grade = :grade ORDER BY distance LIMIT 200; ");
//    			$stmt2->bindValue(':cuisine',$cuisine,PDO::PARAM_STR);
//                $stmt2->bindValue(':grade',$grade,PDO::PARAM_STR);
//    			$stmt2->bindValue(':longitude',$longitude,PDO::PARAM_STR);
//    			$stmt2->bindValue(':latitude',$latitude,PDO::PARAM_STR);
//		    }else{
//    			$stmt2 = $db->prepare( $strSql . " HAVING cuisine = :cuisine and grade = :grade and price=:price ORDER BY distance LIMIT 200; ");
//    			$stmt2->bindValue(':cuisine',$cuisine,PDO::PARAM_STR);
//                $stmt2->bindValue(':sQuery',$searchQuery,PDO::PARAM_STR);
//    			$stmt2->bindValue(':price',$keyword,PDO::PARAM_STR);
//    			$stmt2->bindValue(':grade',$grade,PDO::PARAM_STR);
//    			$stmt2->bindValue(':longitude',$longitude,PDO::PARAM_STR);
//    			$stmt2->bindValue(':latitude',$latitude,PDO::PARAM_STR);
//			}
//
//		}
//	}else{
//		if($keyword != "" && $grade == ""){
//			$stmt2 = $db->prepare( $strSql . " HAVING distance < :distance and price=:price ORDER BY distance LIMIT 200; ");
//			$stmt2->bindValue(':price',$keyword,PDO::PARAM_STR);
//            $stmt2->bindValue(':distance',$distance,PDO::PARAM_STR);
//			$stmt2->bindValue(':longitude',$longitude,PDO::PARAM_STR);
//			$stmt2->bindValue(':latitude',$latitude,PDO::PARAM_STR);
//		}
//		else if($grade != "" && $keyword == ""){
//			$stmt2 = $db->prepare( $strSql . " HAVING distance < :distance and grade = :grade ORDER BY distance LIMIT 200; ");
//			$stmt2->bindValue(':grade',$grade,PDO::PARAM_STR);
//            $stmt2->bindValue(':distance',$distance,PDO::PARAM_STR);
//			$stmt2->bindValue(':longitude',$longitude,PDO::PARAM_STR);
//			$stmt2->bindValue(':latitude',$latitude,PDO::PARAM_STR);
//		}
//		else{
//			$stmt2 = $db->prepare( $strSql . " HAVING distance < :distance and grade = :grade and price=:price ORDER BY distance LIMIT 200; ");
//			$stmt2->bindValue(':grade',$grade,PDO::PARAM_STR);
//            $stmt2->bindValue(':price',$keyword,PDO::PARAM_STR);
//			$stmt2->bindValue(':distance',$distance,PDO::PARAM_STR);
//			$stmt2->bindValue(':longitude',$longitude,PDO::PARAM_STR);
//			$stmt2->bindValue(':latitude',$latitude,PDO::PARAM_STR);
//		}
//	}
//

            // $stmt2->bindValue(':start',$start,PDO::PARAM_INT);
            // $stmt2->bindValue(':end',$end,PDO::PARAM_INT);
            try{
                $stmt2->execute();
                $result = $stmt2->fetchAll(PDO::FETCH_OBJ);
                //$result = $stmt2->fetchAll(PDO::FETCH_ASSOC);
                $outputArray = array();
                $outputArray['price']       = $keyword;
                $outputArray['cuisine']     = $cuisine;
				$outputArray['grade']       = $grade;
				$outputArray['distance']    = $distance;
                $outputArray['latitude']    = $latitude;
                $outputArray['longitude']    = $longitude;

                $outputArray["results"]     = $result;
                showSuccess($outputArray);
                $db = null;
                exit();
            }
            catch(PDOException $e){
                showError("100", $e->getMessage());
                $db = null;
                exit();
            }


        }
        catch(PDOException $e){
            showError("100", $e->getMessage());
            $db = null;
            exit();
        }

        $return["value"] =$outputArray;
        echo json_encode($return);
        $db = null;
        exit();
    }

    function post_charge(){
        include_once("check_session.php");
        //include_once("connect.php");

        include_once "stripe/init.php";


        \Stripe\Stripe::setApiKey("sk_test_3v0DMe1eRY4E83U5fzDHxXR6"); // test
        //\Stripe\Stripe::setApiKey("sk_live_y5J6qXsQWDvch8tz0DoITrlb"); // live

        // Get the credit card details submitted by the form
        $strToken   = $_POST['stripeToken'];
        $fltAmount  = $_POST['amount'];
        $intUserId  = $_POST['user_id'];
        $intRestId  = $_POST['restaurant_id'];
        $strDesc    = isset($_POST['description'])?$_POST['description']:'Charge about your bill';
        $strCur     = isset($_POST['currency'])?$_POST['currency']:'usd';

        // Create the charge on Stripe's servers - this will charge the user's card
        try {
            $charge = \Stripe\Charge::create(array(
              "amount" => $fltAmount, // amount in cents, again
              "currency" => $strCur,
              "source" => $strToken,
              "description" => $strDesc)
            );

            $strSql = 'INSERT INTO payment_transactions(`stripe_token`, `user_id`, `restaurant_id`, `amount`, `currency`, `description`, `created_datetime`)
                VALUES(:stripetoken, :userid, :restaurantid, :amount, :currency, :description, NOW())';
            $objStmt = $db->prepare( $strSql );
            $objStmt->bindValue(':stripetoken',$strToken,PDO::PARAM_STR);
    		$objStmt->bindValue(':userid',$intUserId,PDO::PARAM_STR);
    		$objStmt->bindValue(':restaurantid',$intRestId,PDO::PARAM_STR);
    		$objStmt->bindValue(':amount',$fltAmount,PDO::PARAM_STR);
            $objStmt->bindValue(':description',$strDesc,PDO::PARAM_STR);
            $objStmt->bindValue(':currency',$strCur,PDO::PARAM_STR);
            $objStmt->execute();

            $arrReturn['stripeToken']   = $strToken;
            $arrReturn['amount']        = $fltAmount;
            $arrReturn['user_id']       = $intUserId;
            $arrReturn['restaurant_id'] = $intRestId;
            $arrReturn['currency']      = $strCur;

        } catch(\Stripe\Error\Card $e) {
          // The card has been declined
          header('Content-Type: application/json');
          showError(100, "Payment has been declined");
          return;
        }

        header('Content-Type: application/json');
        showSuccess($arrReturn);

    }
function d($arrData){
    echo '<pre>';print_r($arrData);die;
}


?>
