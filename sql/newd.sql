-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 08, 2016 at 07:07 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `newd`
--

CREATE TABLE `newd` (
  `id` int(9) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `place_id` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `rating` varchar(100) NOT NULL,
  `phone_no` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `grade` varchar(100) NOT NULL,
  `cuisine` varchar(100) NOT NULL,
  `reviews_count` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newd`
--

INSERT INTO `newd` (`id`, `user_id`, `place_id`, `address`, `rating`, `phone_no`, `name`, `latitude`, `longitude`, `grade`, `cuisine`, `reviews_count`) VALUES
(1, '23', '7555332', 'jja ahhhak akaak road', '5', '9855456752', 'ula hababay', '39''55445', '84''55447', 'D', 'ddaaaaadddd,wwwwwwwwdda', '4');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `newd`
--
ALTER TABLE `newd`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `newd`
--
ALTER TABLE `newd`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
